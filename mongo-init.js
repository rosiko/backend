db = db.getSiblingDB(process.env.MONGO_INITDB_DATABASE);

console.log("Creation user " + process.env.MONGO_DB_USER);

db.createUser(
    {
        user: process.env.MONGO_DB_USER,
        pwd: process.env.MONGO_DB_PASSWORD,
        roles: [
            {
                role: "readWrite",
                db: "rosiko-db"
            }
        ]
    }
);