FROM eclipse-temurin:21-alpine AS runner
WORKDIR /app
COPY target/rosiko_be-0.0.1-SNAPSHOT.jar rosiko-be.jar
EXPOSE 8081
ENTRYPOINT ["java", "-jar", "rosiko-be.jar"]