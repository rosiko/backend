package com.cm.rosiko_be.match;

import com.cm.rosiko_be.repositories.MatchRepository;
import com.cm.rosiko_be.services.impl.MatchServiceImpl;
import com.cm.rosiko_be.socket.WebSocketService;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Map;
import java.util.NoSuchElementException;


@RestController
@RequestMapping("/api/v1/match")
@CrossOrigin(origins = "*")
@Deprecated
@Hidden
public class MatchControllerOld {

    @Autowired
    MatchService matchService;

    @Autowired
    MatchRepository matchRepository;

    @Autowired
    MatchServiceImpl newMatchesService;

    @Autowired
    WebSocketService webSocketServices;

    /**
     * Select the territory from which to attack
     * @param json matchId, territoryId
     */
    @PutMapping("/select_attacker")
    public void selectAttacker(@RequestBody Map<String, String> json) {
        Match match = matchRepository.findById(json.get("matchId"))
                .orElseThrow(()->new NoSuchElementException("Match not found"));
        matchService.setMatch(match);
        matchService.selectAttacker(json.get("territoryId"));

        webSocketServices.notifyUpdatedMatch(json.get("matchId"));
    }

    /**
     * Select the territory from which to defend
     * @param json matchId, territoryId
     */
    @PutMapping("/select_defender")
    public void selectDefender(@RequestBody Map<String, String> json) {
        Match match = matchRepository.findById(json.get("matchId"))
                .orElseThrow(()->new NoSuchElementException("Match not found"));
        matchService.setMatch(match);
        matchService.selectDefender(json.get("territoryId"));

        webSocketServices.notifyUpdatedMatch(json.get("matchId"));
    }

    /**
     * Deselect the territory
     * @param json matchId, territoryId
     */
    @PutMapping("/deselect_territory")
    public void deselectTerritory(@RequestBody Map<String, String> json) {
        Match match = matchRepository.findById(json.get("matchId"))
                .orElseThrow(()->new NoSuchElementException("Match not found"));
        matchService.setMatch(match);
        matchService.deselectTerritory(json.get("territoryId"));

        webSocketServices.notifyUpdatedMatch(json.get("matchId"));
    }

    @PostMapping("/attack")
    public void attack(@RequestBody Map<String, String> json) {
        Match match = matchRepository.findById(json.get("matchId"))
                .orElseThrow(()->new NoSuchElementException("Match not found"));
        matchService.setMatch(match);
        matchService.attack(Integer.parseInt(json.get("numberOfAttackerDice")));

        webSocketServices.notifyUpdatedMatch(json.get("matchId"));
    }

    /**
     * Select territory from which to move armies
     * @param json matchId, territoryId
     */
    @PutMapping("/select_territory_from")
    public void selectTerritoryFrom(@RequestBody Map<String, String> json) {
        Match match = matchRepository.findById(json.get("matchId"))
                .orElseThrow(()->new NoSuchElementException("Match not found"));
        matchService.setMatch(match);
        matchService.selectTerritoryFrom(json.get("territoryId"));

        webSocketServices.notifyUpdatedMatch(json.get("matchId"));
    }

    /**
     * Select territory to which to move armies
     * @param json matchId, territoryId
     */
    @PutMapping("/select_territory_to")
    public void selectTerritoryTo(@RequestBody Map<String, String> json) {
        Match match = matchRepository.findById(json.get("matchId"))
                .orElseThrow(()->new NoSuchElementException("Match not found"));
        matchService.setMatch(match);
        matchService.selectTerritoryTo(json.get("territoryId"));

        webSocketServices.notifyUpdatedMatch(json.get("matchId"));
    }

    /**
     * Confirm armies movement
     * @param json matchId, territoryFrom, territoryTo, movedArmies
     */
    @PutMapping("/confirm_move")
    public void confirmMove(@RequestBody Map<String, String> json) {
        Match match = matchRepository.findById(json.get("matchId"))
                .orElseThrow(()->new NoSuchElementException("Match not found"));
        matchService.setMatch(match);
        matchService.displaceArmies(json.get("territoryFrom"), json.get("territoryTo"), Integer.parseInt(json.get("movedArmies")), true);

        webSocketServices.notifyUpdatedMatch(json.get("matchId"));
    }

    /**
     * Change stage to displacement
     * @param json matchId
     */
    @PutMapping("/displacement_stage")
    public void displacementStage(@RequestBody Map<String, String> json) {
        Match match = matchRepository.findById(json.get("matchId"))
                .orElseThrow(()->new NoSuchElementException("Match not found"));

        matchService.setMatch(match);
        matchService.endsAttacks();

        webSocketServices.notifyUpdatedMatch(json.get("matchId"));
    }

    /**
     * Ends turn and save the armies movement
     * @param json matchId
     */
    @PostMapping("/ends_turn")
    public void endsTurn(@RequestBody Map<String, String> json) {
        Match match = matchRepository.findById(json.get("matchId"))
                .orElseThrow(()->new NoSuchElementException("Match not found"));
        matchService.setMatch(match);

        matchService.endsTurn();

        webSocketServices.notifyUpdatedMatch(json.get("matchId"));
    }
}
