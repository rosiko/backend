package com.cm.rosiko_be.match;

import com.cm.openapi.model.*;
import com.cm.rosiko_be.enums.MatchEvent;
import com.cm.rosiko_be.exceptions.GameRulesException;
import com.cm.rosiko_be.map.GameMap;
import com.cm.rosiko_be.map.territory.Territory;
import com.cm.rosiko_be.player.Player;
import com.cm.rosiko_be.utils.ColorUtil;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.*;

import static com.cm.openapi.model.MatchStage.ATTACK;
import static com.cm.openapi.model.MatchStage.INITIAL_PLACEMENT;
import static com.cm.rosiko_be.match.MatchService.MAX_PLAYERS;

@Document
@Data
public class Match {
    @Id
    private String id;
    private String name;
    private String password;
    private MatchState state;
    private MatchEvent event;
    private GameMap map;
    private List<Player> players = new ArrayList<>();
    private String playerOnDutyId;
    private String winnerId;
    private int turn = 0;
    private MatchStage stage;
    private LocalDateTime date;                 //Data di creazione della partita
    private String attackerId;                  //Territorio dell'attaccante
    private String defenderId;                  //Territorio del difensore
    private List<Integer> diceAttacker;         //Risultato del lancio dei dadi dell'attaccante
    private List<Integer> diceDefender;         //Risultato del lancio dei dadi del difensore
    private String territoryFromId;             //Territorio dal quale spostare le armate
    private String territoryToId;               //Territorio dal quale ricevere le armate spostate
    private int moveArmies = 0;                 //Armate da spostare
    private boolean movementConfirmed = false;  //Conferma che il movimento è avvenuto
    private boolean armiesWereAssigned = false; //Conferma che le armate sono state assegnate al giocatore di turno
    private List<Card> cards = new ArrayList<>();

    public Match() {
        super();
        this.date = LocalDateTime.now();
    }

    public Match(String name) {
        this.name = name;
        this.stage = INITIAL_PLACEMENT;
    }


    public Player getPlayerOnDuty(){
        return getPlayer(playerOnDutyId);
    }

    public void setPlayerOnDuty(Player player){
        playerOnDutyId = player.getId();
    }

    public Player getWinner(){
        return getPlayer(winnerId);
    }

    public void setWinner(Player player){
        winnerId = player.getId();
    }

    public void setAttacker(Territory territory) {
        attackerId = territory==null ? null : territory.getId();
    }

    public Territory getAttacker(){
        return getTerritory(attackerId);
    }

    public void setDefender(Territory territory) {
        defenderId = territory==null ? null : territory.getId();
    }

    public Territory getDefender(){
        return getTerritory(defenderId);
    }

    public void setTerritoryFrom(Territory territory) {
        this.territoryFromId = territory == null? null : territory.getId();
    }

    public Territory getTerritoryFrom(){
        return getTerritory(territoryFromId);
    }

    public void setTerritoryTo(Territory territory) {
        this.territoryToId = territory == null? null : territory.getId();
    }

    public Territory getTerritoryTo(){
        return getTerritory(territoryToId);
    }

    public Player getOwner(Territory territory){
        return getPlayer(territory.getOwnerId());
    }

    public void addPlayer(Player player) throws GameRulesException {
        if (players.size() >= MAX_PLAYERS) {
            throw new GameRulesException(String.format("Cannot add new player because max players are %d", MAX_PLAYERS));
        }

        //Inizializza la lista colorsTaken con tutti i colori utilizzati dagli altri giocatori
        List<Color> colorsTaken = new ArrayList<>();
        for (Player currentPlayer : players) {
            colorsTaken.add(currentPlayer.getColor());
        }

        player.setColor(ColorUtil.getRandomColor(colorsTaken));

        players.add(player);
    }

    public void removePlayer(String playerId){
        Player targetPlayer = null;

        for (Player player : players) {
            if(Objects.equals(player.getId(), playerId)){
                targetPlayer = player;
                break;
            }
        }

        players.remove(targetPlayer);
    }

    //Ritorna tutti i continenti posseduti interamente dal player
    public Set<Continent> getContinentsOwned(Player player) {
        Set<Continent> continentsOwned = new HashSet<>();

        for (Continent continent : map.getContinents()) {
            boolean isContinentOwned = true;
            for (Territory territory : map.getTerritories()) {
                //Il proprietario del territorio è diverso dal giocatore
                if (territory.getContinentId().equals(continent.getId())
                        && !player.isTheOwner(territory)
                ) {
                    isContinentOwned = false;
                    break;
                }
            }
            if (isContinentOwned) continentsOwned.add(continent);
        }

        return continentsOwned;
    }

    //Ritorna tutti i territori posseduti dal player
    public Set<Territory> getTerritoriesOwned(Player player) {
        Set<Territory> territoriesOwned = new HashSet<>();

        for (Territory territory : map.getTerritories()) {
            if (player.isTheOwner(territory)) {
                territoriesOwned.add(territory);
            }
        }

        return territoriesOwned;
    }

    public List<Player> getActivePlayers(){
        List<Player> activePlayers = new ArrayList<>();

        for(Player player : players){
            if(player.isActive()) activePlayers.add(player);
        }

        return activePlayers;
    }

    public boolean isFirstTurnForThePlayers(){
        for(Player player : getActivePlayers()){
            if(player.isFirstTurn()) return true;
        }
        return false;
    }

    public void deselectTerritories(){
        attackerId = null;
        defenderId = null;
        territoryFromId = null;
        territoryToId = null;
    }

    public void resetActions() {

        for(Player player : players){
            player.resetActions();
        }

        event = null;
        movementConfirmed = false;
        armiesWereAssigned = false;

        deselectTerritories();
    }

    public Set<Player> getDefeatedPlayersBy(Player player){
        return getPlayers(player.getDefeatedPlayersId());
    }

    public Player findPlayerByColor(Color color){
        for (Player player : players) {
            if(player.getColor().equals(color)) return player;
        }
        return null;
    }

    public Player getPlayer(String id){
        if(id == null) return null;

        for(Player player : players){
            if(id.equals(player.getId())) return player;
        }

        throw new NoSuchElementException(String.format("Player %s not found in match %s", id, this.id));
    }

    public Set<Player> getPlayers(Set<String> ids){
        Set<Player> players = new HashSet<>();

        for(Player player : this.players){
            if(ids.contains(player.getId())){
                players.add(player);
            }
        }

        return players;
    }

    public Territory getTerritory(String id){
        return map.getTerritory(id);
    }

    public boolean isPlayerOnDuty(Player player) {
        return playerOnDutyId != null && playerOnDutyId.equals(player.getId());
    }

    public boolean isActive(){
        boolean hasWinner = winnerId != null;
        boolean hasActivePlayers = !getActivePlayers().isEmpty();
        return hasActivePlayers && !hasWinner;
    }

    public boolean isPlayerInTheMatch(Player player){
        for(Player currentPlayer : players){
            if(player.getId().equals(currentPlayer.getId())) return true;
        }

        return false;
    }

    public List<Player> getActivePlayersWithAvailableArmies(){
        List<Player> playersWithAvailableArmies = new ArrayList<>();

        for (Player player : getActivePlayers()){
            if(player.getAvailableArmies() > 0){
                playersWithAvailableArmies.add(player);
            }
        }

        return playersWithAvailableArmies;
    }

    public void increaseTurn(){
        turn++;
    }

    public void resetEvent() {
        event=null;
    }
}
