package com.cm.rosiko_be.match;

import com.cm.openapi.model.MatchStage;
import com.cm.rosiko_be.map.territory.Territory;
import com.cm.rosiko_be.mission.MissionsService;
import com.cm.rosiko_be.player.Player;
import com.cm.rosiko_be.repositories.MatchRepository;
import com.cm.rosiko_be.services.impl.StageServiceImpl;
import com.cm.rosiko_be.socket.WebSocketService;
import com.cm.rosiko_be.statemachine.StateMachine;
import com.cm.rosiko_be.utils.SelectableTerritoriesUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

import static com.cm.openapi.model.MatchStage.*;
import static com.cm.rosiko_be.enums.MatchEvent.*;
import static java.lang.Math.min;

@Slf4j
@Data
@Service
public class MatchService {

    public static final int MAX_PLAYERS = 6;
    public static final int MIN_PLAYERS = 3;
    public static final int INITIAL_ARMIES_TO_PLACE = 3;    //Numero di armate che si possono piazzare nella fase INITIAL_PLACEMENT
    public static final int TERRITORIES_FOR_AN_ARMY = 3;    //Numero di territori in possesso per avere una armata.
    public static final int MINIMUM_ATTACKING_ARMIES = 2;   //Numero minimo di armate che un territorio deve avere per potere attaccare.
    public static final int MAX_ATTACKING_DICES = 3;        //Numero massimo di dadi concessi all'attaccante.
    public static final int MAX_DEFENDING_DICES = 3;
    public static final int MAX_DICE_VALUE = 6;             //Massimo valore che può assumere un dado.
    public static final int MIN_DICE_VALUE = 1;             //Minimo valore che può assumere un dado.
    public static final int MIN_ARMIES_FOR_TERRITORY = 1;   //Numero minimo di armate possibili per un territorio.
    public static final int TRACTOR_SET_BONUS = 4;          //Bonus di armate nel caso di un tris di trattori
    public static final int FARMER_SET_BONUS = 6;           //Bonus di armate nel caso di un tris di contadini
    public static final int COW_SET_BONUS = 8;              //Bonus di armate nel caso di un tris di mucche
    public static final int DIFFERENT_CARDS_SET_BONUS = 10; //Bonus di armate nel caso di un tris di mucca, contadino e trattore
    public static final int JOLLY_SET_BONUS = 12;           //Bonus di armate nel caso di un tris di un jolly + 2 carte uguali
    public static final int SET_CARDS_NUMBER = 3;           //Numero di carte per fare un tris
    public static final int CARD_TERRITORY_BONUS = 2;       //Numero armate bonus se si possiede il territorio della carta giocata
    public static final int MINIMUM_AVAIABLE_ARMIES = 1;    //Numero armate bonus se si possiede il territorio della carta giocata
    public static final int MAX_INACTIVITY_PERIOD = 100;    //Minuti di inattività consentiti per ogni giocatore
    public static final int NUMBER_OF_JOLLY_IN_THE_DECK = 2;
    public static final int DICE_ROLLING_DELAY = 1000;

    @Autowired
    public WebSocketService webSocketService;

    @Autowired
    public MissionsService missionsService;

    @Autowired
    public SelectableTerritoriesUtil selectableTerritories;

    @Autowired
    public MatchRepository matchRepository;

    @Autowired
    public StageServiceImpl stageService;

    @Autowired
    private StateMachine stateMachine;

    private Match match;

    private final Random random = new Random();

    public MatchService(){
        super();
    }

    public void attack(int numberOfAttackerDice){

        diceRollingDelay(); //todo: remove

        if(match.getAttacker() == null || match.getDefender() == null) {
            match.setDiceAttacker(null);
            match.setDiceDefender(null);
            return;
        }

        List<Integer> dicesAttackerList = getDicesAttacker(numberOfAttackerDice);
        List<Integer> dicesDefenderList = getDicesDefender();
        int armiesLostByAttacker = 0;
        int armiesLostByDefender = 0;

        //Rolling of the dice
        diceRoll(dicesAttackerList);
        diceRoll(dicesDefenderList);

        //Calculation of armies lost by the attacker and defender
        for(int i=0; i<min(dicesAttackerList.size(), dicesDefenderList.size()); i++){
            if( dicesAttackerList.get(i) > dicesDefenderList.get(i) ){
                armiesLostByDefender++;
            }
            else{
                armiesLostByAttacker++;
            }
        }

        match.setDiceAttacker(dicesAttackerList);
        match.setDiceDefender(dicesDefenderList);

        //Decrease in defeated armies
        match.getAttacker().removeArmies(armiesLostByAttacker);
        match.getDefender().removeArmies(armiesLostByDefender);

        //Case of conquest
        conquest(dicesAttackerList.size());

        stateMachine.updateState(match);
        matchRepository.save(match);
    }

    private void diceRollingDelay(){
        try {
            Thread.sleep(DICE_ROLLING_DELAY);
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
        }
    }

    public void selectAttacker(String territoryId){
        Territory territory = match.getMap().getTerritory(territoryId);
        selectAttacker(territory);

        stateMachine.updateState(match);
        matchRepository.save(match);
    }

    private void selectAttacker(Territory territory){
        if(territory == null) return;

        String ownerId = territory.getOwnerId();

        if(match.getPlayerOnDutyId().equals(ownerId) && territory.getPlacedArmies() >= MINIMUM_ATTACKING_ARMIES){
            match.setAttacker(territory);
            match.setDiceAttacker(null);
            match.setDefender(null);
            match.setDiceDefender(null);
            match.setTerritoryTo(null);
            match.setTerritoryFrom(null);
        }
    }

    //Seleziona un proprio territorio dal quale sia possibile attaccare
    public void selectDefender(String territoryId){
        Territory territory = match.getTerritory(territoryId);

        //Se il territorio è confinante con l'attaccante setta il territorio come difensore
        if(territory.isBordering(match.getAttacker())){
            match.setDefender(territory);
            match.setDiceDefender(null);
            match.setTerritoryTo(null);
            match.setTerritoryFrom(null);
        }

        stateMachine.updateState(match);
        matchRepository.save(match);
    }

    //Deseleziona un territorio che sia quello attaccante o quello difensivo
    public void deselectTerritory(String territoryId){
        //Se il territorio è quello attaccante lo deseleziona e deseleziona pure quello difensivo
        if(match.getAttacker()!= null && match.getAttacker().getId().equals(territoryId)){
            if(
                    match.getDefender() != null
                    && match.getDefender().getOwnerId().equals(match.getAttacker().getOwnerId())
                    && match.getDefender().getPlacedArmies() >= MINIMUM_ATTACKING_ARMIES
            ){
                match.setAttacker(match.getDefender());
            }
            else{
                match.setAttacker(null);
            }
            match.setDefender(null);
        }

        //Se il territorio è quello difensivo lo deseleziona
        if(match.getDefender()!= null && match.getDefender().getId().equals(territoryId)){
            match.setDefender(null);
        }

        //Se il territorio è quello dal quale spostare le armate deseleziona pure quello di destinazione
        if(match.getTerritoryFrom()!= null && match.getTerritoryFrom().getId().equals(territoryId)){
            match.setTerritoryFrom(null);
            match.setTerritoryTo(null);
        }

        //Se il territorio è quello al quale destinare le armate lo deseleziona
        if(match.getTerritoryTo()!= null && match.getTerritoryTo().getId().equals(territoryId)){
            match.setTerritoryTo(null);
        }

        stateMachine.updateState(match);
        matchRepository.save(match);
    }

    //Seleziona il territorio dal quale spostare le armate
    public void selectTerritoryFrom(String territoryId){
        Territory territory = match.getMap().getTerritory(territoryId);
        if(territory.getOwnerId().equals(match.getPlayerOnDutyId()) && territory.getPlacedArmies() > MIN_ARMIES_FOR_TERRITORY){
            match.setTerritoryFrom(territory);
            match.setDefender(null);
        }

        stateMachine.updateState(match);
        matchRepository.save(match);
    }

    //Seleziona il territorio sul quale spostare le armate
    public void selectTerritoryTo(String territoryId){
        Territory territory = match.getMap().getTerritory(territoryId);

        if(     territory.getOwnerId().equals(match.getPlayerOnDutyId())
                && match.getTerritoryFrom() != null
                && territory.getOwnerId().equals(match.getTerritoryFrom().getOwnerId())){
            if(territory.isBordering(match.getTerritoryFrom())) {
                match.setTerritoryTo(territory);
                match.setDefender(null);
            }
            else match.setTerritoryFrom(territory);
        }

        stateMachine.updateState(match);
        matchRepository.save(match);
    }

    //Ritorna un array con i dadi a disposizione del difensore
    private List<Integer> getDicesDefender(){
        List<Integer> listDices = new ArrayList<>();
        Territory defender = match.getDefender();
        int armies = 0;
        int numDices = 0;

        if(defender != null) armies = defender.getPlacedArmies();
        numDices = Math.min(armies, MAX_ATTACKING_DICES);

        for(int i=0; i<numDices; i++) listDices.add(5);

        return listDices;
    }

    //Ritorna la lista di dadi disponibili per l'attacco
    private List<Integer> getDicesAttacker(int numberOfAttackerDice) {
        List<Integer> listDices = new ArrayList<>();
        int attackerArmies = match.getAttacker().getPlacedArmies();
        int maxDiceAvailable = min(attackerArmies - MIN_ARMIES_FOR_TERRITORY, MAX_ATTACKING_DICES);

        for(int i=0; i<min(maxDiceAvailable, numberOfAttackerDice); i++){
            listDices.add(5);
        }

        return listDices;
    }

    //Lancia i dadi passati come parametro e li ordina dal valore maggiore al minore
    private void diceRoll(List<Integer> diceList){
        diceList.replaceAll(ignored -> random.nextInt(MAX_DICE_VALUE) + MIN_DICE_VALUE);

        Collections.sort(diceList);
        Collections.reverse(diceList);
    }

    //Nel caso in cui l'attaccante sconfiggesse tutte le armate del difensore allora conquista il territorio attaccato
    private void conquest(int attackArmies){
        Territory attackerTerritory = match.getAttacker();
        Territory defenderTerritory = match.getDefender();
        Player player = match.getPlayerOnDuty();
        Player defender = match.getOwner(defenderTerritory);

        /*Se il difensore non ha più armate, l'attaccante diventa il proprietario del territorio
        e ci vengono trasferite le armate che hanno attaccato*/
        if(defenderTerritory.getPlacedArmies() <= 0){
            defenderTerritory.setOwner(match.getOwner(attackerTerritory));

            //Set the minimum movement
            match.setTerritoryFrom(match.getAttacker());
            match.setTerritoryTo(match.getDefender());
            moveArmies(match.getTerritoryFrom(), match.getTerritoryTo(), attackArmies, false);

            player.setMustDrawACard(true);     //A fine turno il giocatore potrà pescare una carta
            match.setMovementConfirmed(false);

            //Controlla se il giocatore sconfitto ha perso tutti i territori
            Set<Territory> territories = match.getTerritoriesOwned(defender);
            if(territories.isEmpty()){
                player.addDefeatedPlayer(defender);
                player.addCards(defender.takeCards());
                defender.setActive(false);
            }

            //Controlla se c'è un vincitore
            stageService.calculateWinner(match);
        }
    }


    //Fase di spostamento delle armate, indica le armate da spostare
    public void displaceArmies(String territoryFromId, String territoryToId, int armies, boolean deselectAfterMove){
        Territory territoryFrom = match.getMap().getTerritory(territoryFromId);
        Territory territoryTo = match.getMap().getTerritory(territoryToId);
        MatchStage stage = match.getStage();

        moveArmies(territoryFrom, territoryTo, armies, deselectAfterMove);

        //Dopo lo sopostamento, se è in fase di attacco seleziona come attaccante per la porssima fase il territorio con più armate che è stato coinvolto nello spostamento
        if(ATTACK.equals(stage)){
            if(territoryFrom.getPlacedArmies() > territoryTo.getPlacedArmies()){
                selectAttacker(territoryFrom);
            }
            else selectAttacker(territoryTo);
        }
        else if (DISPLACEMENT.equals(stage)) {
            match.setEvent(ARMIES_DISPLACED);
        }

        stateMachine.updateState(match);
        matchRepository.save(match);
    }

    //Sposta le armate da un territorio a un altro
    private void moveArmies(Territory territoryFrom, Territory territoryTo, int armies, boolean deselectAfterMove){
        //Sposta solo se i territori sono confinanti e se il numero di armate è coerente
        if( territoryFrom != null
            && territoryTo != null
            && territoryFrom.isBordering(territoryTo)
            && territoryFrom.getPlacedArmies() - armies >= MIN_ARMIES_FOR_TERRITORY
            && territoryTo.getPlacedArmies() + armies >= MIN_ARMIES_FOR_TERRITORY
            && armies >= 0
        ) {

            territoryFrom.removeArmies(armies);
            territoryTo.addArmies(armies);

            //Se il flag è true deseleziona tutti i territori
            if(deselectAfterMove) match.deselectTerritories();

            match.setMovementConfirmed(true);
        }
    }

    public void endsTurn() {
        match.setEvent(END_TURN);
        stateMachine.updateState(match);
        matchRepository.save(match);
    }

    public void endsAttacks() {
        match.setEvent(END_ATTACKS);
        match.deselectTerritories();
        stateMachine.updateState(match);
        matchRepository.save(match);
    }
}
