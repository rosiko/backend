package com.cm.rosiko_be.controllers;

import com.cm.openapi.api.MatchesApi;
import com.cm.openapi.model.*;
import com.cm.rosiko_be.services.MatchService;
import com.cm.rosiko_be.socket.WebSocketService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import java.util.List;
import java.util.Set;

import static org.springframework.http.ResponseEntity.ok;

@Controller
@RequiredArgsConstructor
public class MatchController implements MatchesApi {

    private final MatchService matchService;
    private final WebSocketService webSocketService;

    @Override
    public ResponseEntity<List<MatchDto>> getMatches(Set<MatchState> status) {
        List<MatchDto> matches = matchService.getMatches(status);
        return ok(matches);
    }

    @Override
    public ResponseEntity<MatchDto> getMatch(String id) {
        return ok(matchService.getMatch(id));
    }

    @Override
    public ResponseEntity<MatchDto> newMatch(NewMatchRequest request){
        MatchDto match = matchService.newMatch(request);
        webSocketService.notifyAvailableMatches();
        return ok(match);
    }

    @Override
    public ResponseEntity<MatchState> getMatchState(String id) {
        MatchState matchState = matchService.getMatch(id).getState();
        return ok(matchState);
    }

    @Override
    public ResponseEntity<Void> startMatch(String id) {
        matchService.startMatch(id);
        webSocketService.notifyAvailableMatches();
        webSocketService.notifyUpdatedMatch(id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> playCards(String id, Set<Long> requestBody) {
        matchService.playCards(id, requestBody);
        webSocketService.notifyUpdatedMatch(id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<String> placeArmies(String id, Set<ArmiesToPlaceDto> armiesToPlace) {
        matchService.placeArmies(id, armiesToPlace);
        webSocketService.notifyUpdatedMatch(id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> updateMatch(String id, MatchDto match) {
        return MatchesApi.super.updateMatch(id, match);
    }

    @Override
    public ResponseEntity<Void> armiesMovement(String id, ArmiesMovement armiesMovement) {
        return MatchesApi.super.armiesMovement(id, armiesMovement);
    }

    @Override
    public ResponseEntity<Void> attack(String id, Integer numberOfDices) {
        return MatchesApi.super.attack(id, numberOfDices);
    }

    @Override
    public ResponseEntity<Void> deselectTerritory(String id, DeselectTerritoryRequest request) {
        return MatchesApi.super.deselectTerritory(id, request);
    }

    @Override
    public ResponseEntity<Void> displacementStage(String id) {
        return MatchesApi.super.displacementStage(id);
    }

    @Override
    public ResponseEntity<Void> endsTurn(String id) {
        return MatchesApi.super.endsTurn(id);
    }

    @Override
    public ResponseEntity<List<PlayerDto>> getPlayers(String id) {
        return MatchesApi.super.getPlayers(id);
    }

    @Override
    public ResponseEntity<Analytical> getAnalytical() {
        Analytical status = matchService.getAnalytical();
        return ok(status);
    }

    @Override
    public ResponseEntity<Void> joinMatch(String id) {
        matchService.joinMatch(id);
        webSocketService.notifyAvailableMatches();
        webSocketService.notifyUpdatedMatch(id);
        return ok().build();
    }

    @Override
    public ResponseEntity<Void> leavesMatch(String id) {
        matchService.leavesMatch(id);
        webSocketService.notifyAvailableMatches();
        webSocketService.notifyUpdatedMatch(id);

        return ok().build();
    }

    @Override
    public ResponseEntity<Void> selectTerritory(String id, SelectTerritoryRequest selectTerritoryRequest) {
        return MatchesApi.super.selectTerritory(id, selectTerritoryRequest);
    }
}
