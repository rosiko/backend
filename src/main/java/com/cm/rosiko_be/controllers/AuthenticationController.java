package com.cm.rosiko_be.controllers;

import com.cm.openapi.api.AuthApi;
import com.cm.openapi.model.*;
import com.cm.rosiko_be.services.impl.AuthenticationServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

@Controller
@RequiredArgsConstructor
public class AuthenticationController implements AuthApi {

    private final AuthenticationServiceImpl authenticationService;

    @Override
    public ResponseEntity<Void> register(RegistrationRequest registrationRequest) {
        authenticationService.register(registrationRequest);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<AuthenticationResponse> authenticate(AuthenticationRequest authenticationRequest) {
        return ResponseEntity.ok(authenticationService.authenticate(authenticationRequest));
    }

    @Override
    public ResponseEntity<Void> verifyAccount(AccountVerificationRequest accountVerificationRequest) {
        authenticationService.verifyAccount(accountVerificationRequest);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> initiateResetPassword(String email) {
        authenticationService.initiateResetPassword(email);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> resetPassword(ResetPasswordRequest resetPasswordRequest) {
        authenticationService.resetPassword(resetPasswordRequest);
        return ResponseEntity.ok().build();
    }
}
