package com.cm.rosiko_be.controllers;

import com.cm.openapi.api.UserApi;
import com.cm.openapi.model.MatchDto;
import com.cm.openapi.model.UpdateUserRequest;
import com.cm.openapi.model.UserDto;
import com.cm.rosiko_be.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

@Controller
@RequiredArgsConstructor
public class UsersController implements UserApi {

    private final UserService userService;

    @Override
    public ResponseEntity<UserDto> getUser() {
        return ResponseEntity.ok().body(userService.getUserDto());
    }

    @Override
    public ResponseEntity<Void> updateUser(UpdateUserRequest updateUserRequest) {
        userService.updateUser(updateUserRequest);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<MatchDto> getUserCurrentMatch() {
        return ResponseEntity.ok(userService.getUserCurrentMatch());
    }
}
