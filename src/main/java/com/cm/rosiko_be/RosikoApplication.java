package com.cm.rosiko_be;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@Slf4j
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class RosikoApplication {

	public static void main(String[] args) {
		log.info("Email: " + System.getenv("EMAIL"));
		SpringApplication.run(RosikoApplication.class, args);
	}
}
