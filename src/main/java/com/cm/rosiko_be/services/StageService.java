package com.cm.rosiko_be.services;

import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.player.Player;

import java.util.List;
import java.util.Optional;

public interface StageService {

    void newTurn(Match match);

    void endTurn(Match match);

    void calculateWinner(Match match);

    void assignArmiesToThePlayer(Match match);

    void prepareAttackStage(Match match);
}
