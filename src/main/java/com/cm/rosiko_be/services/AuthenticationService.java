package com.cm.rosiko_be.services;

import com.cm.openapi.model.*;
import org.springframework.http.ResponseEntity;

public interface AuthenticationService {
    AuthenticationResponse authenticate(AuthenticationRequest request);

    void register(RegistrationRequest request);

    void verifyAccount(AccountVerificationRequest request);

    void initiateResetPassword(String email);

    void resetPassword(ResetPasswordRequest resetPasswordRequest);
}
