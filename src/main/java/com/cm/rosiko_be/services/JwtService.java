package com.cm.rosiko_be.services;

import com.cm.rosiko_be.model.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Map;

public interface JwtService {

    String generateToken(User user, Map<String, Object> extraClaims);

    String generateToken(User user);

    boolean isTokenValid(UserDetails user, String token);

    String extractEmail(String token);
}
