package com.cm.rosiko_be.services;

import com.cm.rosiko_be.match.MatchService;
import java.util.Timer;
import java.util.TimerTask;

public class TimerService {

    private Timer timer = null;
    private static final long DELAY = MatchService.MAX_INACTIVITY_PERIOD * 60000L;


    public void startTimer(TimerTask task, long delay){
        if(timer != null) timer.cancel();
        timer = new Timer();
        timer.schedule(task, delay);
    }

    public void stopTimer(){
        if(timer != null) timer.cancel();
    }
}
