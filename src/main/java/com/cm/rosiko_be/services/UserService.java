package com.cm.rosiko_be.services;

import com.cm.openapi.model.MatchDto;
import com.cm.openapi.model.UpdateUserRequest;
import com.cm.openapi.model.UserDto;
import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface UserService extends UserDetailsService {

    User getUser();

    UserDto getUserDto();

    User loadUserByEmail(String email) throws UsernameNotFoundException;

    void setCurrentMatch(Match match);

    void removeCurrentMatch(User user);

    void updateUser(UpdateUserRequest request);

    MatchDto getUserCurrentMatch();
}
