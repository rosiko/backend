package com.cm.rosiko_be.services;

import com.cm.openapi.model.*;
import com.cm.rosiko_be.match.Match;
import java.util.List;
import java.util.Set;

public interface MatchService {

    Analytical getAnalytical();

    List<MatchDto> getMatches(Set<MatchState> state);

    MatchDto newMatch(NewMatchRequest newMatchData);

    MatchDto getMatch(String id);

    void joinMatch(String matchId);

    void leavesMatch(String matchId);

    void startMatch(String matchId);

    void updateMatch(Match match);

    void playCards(String matchId, Set<Long> cardsId);

    void placeArmies(String matchId, Set<ArmiesToPlaceDto> armiesToPlace);
}
