package com.cm.rosiko_be.services.impl;

import com.cm.openapi.model.*;
import com.cm.rosiko_be.exceptions.AccountNotVerifiedException;
import com.cm.rosiko_be.exceptions.RegistrationException;
import com.cm.rosiko_be.exceptions.TokenException;
import com.cm.rosiko_be.model.Token;
import com.cm.rosiko_be.model.User;
import com.cm.rosiko_be.repositories.UserRepository;
import com.cm.rosiko_be.services.AuthenticationService;
import com.cm.rosiko_be.services.EmailService;
import com.cm.rosiko_be.services.JwtService;
import com.cm.rosiko_be.utils.EmailUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.util.NoSuchElementException;
import java.util.UUID;

import static com.cm.rosiko_be.enums.Role.USER;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    @Value("${frontend_url}")
    private String frontendUrl;

    @Value("${security.temporary_token.validity_in_hours}")
    private int temporaryTokenValidity;

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final EmailService emailService;

    @Override
    public void register(RegistrationRequest request) {

        validateRegisterRequest(request);

        User user = new User();
        user.setName(request.getName());
        user.setEmail(request.getEmail());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setRole(USER);
        user.setTemporaryToken(buildToken());

        userRepository.save(user);

        emailService.sendAccountVerificationEmail(user, getAccountVerificationRedirectUrl(user));
    }

    @Override
    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                request.getEmail(),
                request.getPassword()
        );

        authenticationManager.authenticate(authenticationToken);

        User user = userRepository.findByEmail(request.getEmail()).orElseThrow();

        if(!user.isActivated()){
            emailService.sendAccountVerificationEmail(user, getAccountVerificationRedirectUrl(user));
            throw new AccountNotVerifiedException("User is not verified, check e-mails");
        }

        String jwtToken = jwtService.generateToken(user);
        AuthenticationResponse response = new AuthenticationResponse();
        response.setToken(jwtToken);

        return response;
    }

    @Override
    public void verifyAccount(AccountVerificationRequest request) {
        User user = userRepository.findById(request.getUserId())
                .orElseThrow(() -> new NoSuchElementException("User not found with id " + request.getUserId()));

        validateToken(user, request.getTemporaryToken());

        user.setActivated(true);
        user.deleteTemporaryToken();
        userRepository.save(user);
    }

    @Override
    public void initiateResetPassword(String email) {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new NoSuchElementException("No user found with email " + email));

        user.setTemporaryToken(buildToken());
        userRepository.save(user);

        emailService.sendResetPasswordEmail(user, getResetPasswordRedirectUrl(user));
    }

    @Override
    public void resetPassword(ResetPasswordRequest request) {
        User user = userRepository.findById(request.getUserId())
                .orElseThrow(() -> new NoSuchElementException("No user found"));

        validateToken(user, request.getTemporaryToken());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.deleteTemporaryToken();
        userRepository.save(user);
    }

    private String getAccountVerificationRedirectUrl(User user){
        Token token = user.getTemporaryToken();

        try { validateToken(token); }
        catch (TokenException ex){
            token = buildToken();
            user.setTemporaryToken(token);
            userRepository.save(user);
        }

        return String.format("%s/account-verification/?id=%s&token=%s",
                frontendUrl,
                user.getId(),
                user.getTemporaryToken().getValue()
        );
    }

    private String getResetPasswordRedirectUrl(User user){
        return String.format("%s/reset-password/?id=%s&token=%s",
                frontendUrl,
                user.getId(),
                user.getTemporaryToken().getValue()
        );
    }

    private void validateRegisterRequest(RegistrationRequest request) {
        if(!EmailUtil.validate(request.getEmail())){
            throw new IllegalArgumentException("Email is not valid");
        }
        if(userRepository.findByEmail(request.getEmail()).isPresent()){
            throw new RegistrationException("The email you entered is already associated with an account");
        }
    }

    private Token buildToken(){
        Token token = new Token();
        token.setValue(UUID.randomUUID().toString());
        token.setExpirationDate(LocalDateTime.now().plusHours(temporaryTokenValidity));
        return token;
    }

    private void validateToken(User user, String token){
        Token userToken = user.getTemporaryToken();

        validateToken(userToken);

        if(!userToken.getValue().equals(token)){
            throw new TokenException("Token is not valid");
        }
    }

    private void validateToken(Token token){
        if(token == null){
            throw new TokenException("Token is missing");
        }

        LocalDateTime expirationDate = token.getExpirationDate();

        if(expirationDate == null || expirationDate.isBefore(LocalDateTime.now())){
            throw new TokenException("Token is expired");
        }
    }
}
