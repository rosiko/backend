package com.cm.rosiko_be.services.impl;

import com.cm.rosiko_be.dao.GameMapDao;
import com.cm.rosiko_be.model.ArmiesToPlace;
import com.cm.rosiko_be.exceptions.GameRulesException;
import com.cm.openapi.model.*;
import com.cm.rosiko_be.map.territory.Territory;
import com.cm.rosiko_be.mappers.ArmiesToPlaceMapper;
import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.mission.Mission;
import com.cm.rosiko_be.mission.MissionsService;
import com.cm.rosiko_be.model.User;
import com.cm.rosiko_be.player.Player;
import com.cm.rosiko_be.repositories.MatchRepository;
import com.cm.rosiko_be.services.MatchService;
import com.cm.rosiko_be.services.StageService;
import com.cm.rosiko_be.services.UserService;
import com.cm.rosiko_be.statemachine.StateMachine;
import com.cm.rosiko_be.utils.CardsUtil;
import com.cm.rosiko_be.utils.RulesUtil;
import com.cm.rosiko_be.utils.TimeUtil;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import java.util.*;

import static com.cm.openapi.model.CardType.*;
import static com.cm.openapi.model.MatchStage.*;
import static com.cm.openapi.model.MatchState.*;
import static com.cm.rosiko_be.enums.MatchEvent.START_MATCH;
import static com.cm.rosiko_be.mappers.MatchMapper.MATCH_MAPPER;
import static java.lang.String.format;

@Slf4j
@Service
@RequiredArgsConstructor
public class MatchServiceImpl implements MatchService {

    private final MatchRepository matchRepository;
    private final UserService userService;
    private final RulesUtil rulesUtil;
    private final MissionsService missionsService;
    private final GameMapDao gameMapDao;
    private final TimeUtil timeUtil;
    private final CardsUtil cardsUtil;
    private final StateMachine stateMachine;
    private final StageService stageService;

    @Override
    public Analytical getAnalytical(){
        Analytical analytical = new Analytical();

        List<Match> waitingMatches = matchRepository.findByState(WAITING);
        List<Match> readyMatches = matchRepository.findByState(READY);
        List<Match> activeMatches = matchRepository.findByState(STARTED);

        analytical.setWaitingMatches( waitingMatches.size() );
        analytical.setReadyMatches( readyMatches.size() );
        analytical.setActiveMatches( activeMatches.size() );

        analytical.setWaitingPlayers( countActivePlayers(waitingMatches) );
        analytical.setReadyMatches( countActivePlayers(readyMatches) );
        analytical.setActivePlayers( countActivePlayers(activeMatches) );

        return analytical;
    }

    @Override
    public List<MatchDto> getMatches(Set<MatchState> states) {
        List<Match> matches;

        if(states == null || states.isEmpty()){
            matches = matchRepository.findAll();
        }
        else{
            matches = matchRepository.findByStateIn(states);
        }

        return MATCH_MAPPER.toMatchDto(matches);
    }

    @Override
    public MatchDto newMatch(NewMatchRequest request) {
        User user = userService.getUser();

        Match match = new Match();
        match.setName(request.getName());
        match.setPassword(request.getPassword());
        match.setMap(gameMapDao.getGameMap());
        match.setDate(timeUtil.now());
        joinMatch(match, user);

        stateMachine.updateState(match);
        match = matchRepository.insert(match);
        userService.setCurrentMatch(match);

        return MATCH_MAPPER.toMatchDto(match);
    }

    @Override
    public MatchDto getMatch(String id){
        Match match = loadMatch(id);
        return MATCH_MAPPER.toMatchDto(match);
    }

    @Override
    public void joinMatch(String matchId) {
        User user = userService.getUser();
        Match match = loadMatch(matchId);

        joinMatch(match, user);

        stateMachine.updateState(match);
        match = matchRepository.save(match);
        userService.setCurrentMatch(match);
    }

    @Override
    public void leavesMatch(@NotNull String matchId){
        User user = userService.getUser();
        Match currentMatch = user.getCurrentMatch();

        if(currentMatch == null){
            log.warn("It's not possible leave the match. User hasn't match associated");
            return;
        }

        if(currentMatch.getId().equals(matchId)){
            userService.removeCurrentMatch(user);
        }

        if(STARTED.equals(currentMatch.getState())){
            Player player = currentMatch.getPlayer(user.getId());
            player.setActive(false);
            stageService.calculateWinner(currentMatch);
        }
        else{
            currentMatch.removePlayer(user.getId());
        }

        stateMachine.updateState(currentMatch);
        matchRepository.save(currentMatch);
    }

    @Override
    public void updateMatch(Match match){
        stateMachine.updateState(match);
        matchRepository.save(match);
    }

    @Override
    public void startMatch(String id){
        Match match = loadMatch(id);

        match.setEvent(START_MATCH);

        sortPlayersRandomly(match);

        assignsArmiesToPlayers(match);

        assignMissionsToPlayers(match);

        assignTerritoriesToPlayers(match);

        setDeckOfCards(match);

        placeMinimumArmiesOnEachTerritory(match);

        stateMachine.updateState(match);
        matchRepository.save(match);
    }

    @Override
    public void playCards(String matchId, Set<Long> cardsId){
        Match match = loadMatch(matchId);
        Player player = match.getPlayerOnDuty();
        Set<Card> playerCards = new HashSet<>();
        MatchStage stage = match.getStage();
        int availableArmies = player.getAvailableArmies();
        int armiesBonus;

        if(!PLACEMENT.equals(stage)){
            throw new GameRulesException(
                    format("It's not possible to play cards in the %s stage of the match", stage.name().toLowerCase())
            );
        }

        for(long cardId : cardsId){
            playerCards.add(player.getCard(cardId));
        }

        armiesBonus = cardsUtil.getArmiesBonus(match, playerCards);

        if(armiesBonus > 0){
            for(long cardId : cardsId){
                returnCardToDeck(match, player, cardId);
            }

            player.setAvailableArmies(availableArmies + armiesBonus);
        }

        stateMachine.updateState(match);
        matchRepository.save(match);
    }

    @Override
    public void placeArmies(String matchId, Set<ArmiesToPlaceDto> armiesToPlaceDtoSet){
        Match match = loadMatch(matchId);
        Set<ArmiesToPlace> armiesToPlacesSet = ArmiesToPlaceMapper.toArmiesToPlace(armiesToPlaceDtoSet, match.getMap());
        placeArmies(match, armiesToPlacesSet);

        stateMachine.updateState(match);
        matchRepository.save(match);
    }

    private void joinMatch(Match match, User user) {
        if(user.getCurrentMatch() != null){
            leavesMatch(user.getCurrentMatchId());
        }

        Player player = new Player(user.getId(), user.getName());

        if(match.isPlayerInTheMatch(player)){
            log.warn(format("User %s is already registered to the match %s", user.getId(), match.getId()));
            throw new GameRulesException("User can't join the same match 2 times");
        }
        else{
            match.addPlayer(player);
        }
    }

    private void placeArmies(Match match, Set<ArmiesToPlace> armiesToPlaceSet){
        validateArmiesPlacement(match, armiesToPlaceSet);

        for(ArmiesToPlace armiesToPlace : armiesToPlaceSet){
            placeArmies(match, armiesToPlace);
        }
    }

    private void placeArmies(Match match, ArmiesToPlace armiesToPlace){
        Territory territory = armiesToPlace.getTerritory();
        Player player = match.getOwner(territory);
        int numberOfArmiesToPlace = armiesToPlace.getQuantity();

        player.decreaseAvailableArmies(numberOfArmiesToPlace);
        player.increaseArmiesPlacedThisTurn(numberOfArmiesToPlace);
        territory.addArmies(numberOfArmiesToPlace);
    }

    private void validateArmiesPlacement(Match match, Set<ArmiesToPlace> armiesToPlaceSet){
        MatchStage currentStage = match.getStage();
        Player player = match.getPlayerOnDuty();
        int armiesCounter = 0;
        int availableArmies = player.getAvailableArmies();

        if(!PLACEMENT.equals(currentStage) && !INITIAL_PLACEMENT.equals(currentStage)){
            throw new GameRulesException(
                    format("It's not possible to place armies in the %s stage", currentStage.name().toLowerCase())
            );
        }

        for(ArmiesToPlace armiesToPlace : armiesToPlaceSet){
            armiesCounter += armiesToPlace.getQuantity();
            Territory territory = armiesToPlace.getTerritory();

            if(!player.getId().equals(territory.getOwnerId())){
                throw new GameRulesException(
                        format("%s not belong to the player", territory.getName())
                );
            }
        }

        if(armiesCounter > availableArmies){
            throw new GameRulesException("The player hasn't enough armies");
        }
    }

    private void returnCardToDeck(Match match, Player player, long cardId){
        Card cardToRemove= null;

        for(Card card : player.getCards()){
            if(cardId == card.getId()){
                cardToRemove = card;
                break;
            }
        }

        if(cardToRemove == null) throw new GameRulesException(
                format("Player %s hasn't the card %s to return to deck", player.getId(), cardId)
        );

        player.removeCard(cardToRemove);
        match.getCards().add(cardToRemove);
    }

    private int countActivePlayers(List<Match> matches){
        int activePlayers = 0;

        for(Match match : matches){
            activePlayers += match.getActivePlayers().size();
        }

        return activePlayers;
    }

    private void sortPlayersRandomly(Match match){
        List<Player> players = match.getPlayers();
        Collections.shuffle(players);
        match.setPlayerOnDuty(players.getFirst());
    }

    private void assignsArmiesToPlayers(Match match){
        List<Player> players = match.getPlayers();
        int armies = 50 - players.size() * 5;

        for (Player player : match.getPlayers()) {
            player.setAvailableArmies(armies);
        }
    }

    private void assignMissionsToPlayers(Match match){
        int index = 0;
        List<Mission> missions = missionsService.getMissions();

        Collections.shuffle(missions);

        for(Player player : match.getPlayers()){
            player.setMission(missions.get(index));
            index++;
        }
    }

    private void assignTerritoriesToPlayers(Match match){
        int playerIndex = 0;
        List<Territory> territories = new ArrayList<>(match.getMap().getTerritories());
        List<Player> players = match.getPlayers();

        Collections.shuffle(territories);

        for(Territory territory : territories){
            Player player = players.get(playerIndex);
            territory.setOwner(player);
            playerIndex++;
            if(playerIndex>=players.size()) playerIndex = 0;
        }
    }

    private void setDeckOfCards(Match match){
        List<Card> cards = new ArrayList<>();
        Set<Territory> territories = match.getMap().getTerritories();

        long index=0L;
        for (Territory territory : territories) {
            Card card = new Card();
            card.setId(index);
            card.setTerritoryId(territory.getId());
            card.setCardType(territory.getCardType());
            cards.add(card);
            index++;
        }

        for(int i=0; i<rulesUtil.getNumberOfJolly(); i++){
            Card card = new Card();
            card.setId(index);
            card.setCardType(JOLLY);
            cards.add(card);
            index++;
        }

        match.setCards(cards);
    }

    private void placeMinimumArmiesOnEachTerritory(Match match){
        Set<Territory> territories = match.getMap().getTerritories();
        int minimumArmiesInTerritories = rulesUtil.getMinimumArmiesInTerritories();

        for(Territory territory : territories){
            if(territory.getPlacedArmies() < minimumArmiesInTerritories){
                ArmiesToPlace armiesToPlace = new ArmiesToPlace();
                armiesToPlace.setTerritory(territory);
                armiesToPlace.setQuantity(minimumArmiesInTerritories - territory.getPlacedArmies());
                placeArmies(match, armiesToPlace);
            }
        }
    }

    public void selectAttacker(Match match, String territoryId){
        Territory territory = match.getMap().getTerritory(territoryId);
        selectAttacker(match, territory);

        matchRepository.save(match);
    }

    private void selectAttacker(Match match, Territory territory){
        String ownerId = territory.getOwnerId();

        if(match.getPlayerOnDutyId().equals(ownerId) && territory.getPlacedArmies() >= rulesUtil.getMinimumAttackingArmies()){
            match.setAttacker(territory);
            match.setDefender(null);
            match.setTerritoryTo(null);
            match.setTerritoryFrom(null);
        }
    }

    public void setPlayerInactive(Match match){
        Player player = match.getPlayerOnDuty();
        player.setActive(false);
        //turnService.endTurn(match);
        //if(match.getWinner() == null) wsServices.sendsUpdatedMatchToPlayers(match.getId());
    }

    private Match loadMatch(String id){
        return matchRepository.findById(id)
                .orElseThrow(()-> new NoSuchElementException(format("Match %s not found", id)));
    }
}
