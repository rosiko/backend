package com.cm.rosiko_be.services.impl;

import com.cm.openapi.model.Card;
import com.cm.openapi.model.Continent;
import com.cm.openapi.model.MatchStage;
import com.cm.rosiko_be.map.territory.Territory;
import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.player.Player;
import com.cm.rosiko_be.services.StageService;
import com.cm.rosiko_be.utils.RulesUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.cm.openapi.model.MatchStage.*;
import static com.cm.rosiko_be.enums.MatchEvent.NEW_TURN;

@Service
@RequiredArgsConstructor
public class StageServiceImpl implements StageService {

    private final RulesUtil rulesUtil;

    @Override
    public void newTurn(Match match) {
        MatchStage stage = match.getStage();

        if(!INITIAL_PLACEMENT.equals(stage)){
            match.increaseTurn();
        }
        if(INITIAL_PLACEMENT.equals(stage)){
            match.resetActions();
        }
        if(PLACEMENT.equals(stage)){
            assignArmiesToThePlayer(match);
        }
    }

    @Override
    public void endTurn(Match match) {
        drawACard(match, match.getPlayerOnDuty());
        setNewPlayerOnDuty(match);
        match.resetActions();
        match.setEvent(NEW_TURN);
    }

    @Override
    public void calculateWinner(Match match){
        List<Player> activePlayers = match.getActivePlayers();

        if(activePlayers.size() == 1){
            match.setWinner(activePlayers.getFirst());
        }
        else{
            for(Player player : activePlayers) {
                if(player.getMission().isMissionCompleted(player, match)) {
                    match.setWinner(player);
                }
            }
        }
    }

    @Override
    public void prepareAttackStage(Match match){
        Territory territoryFrom = match.getTerritoryFrom();
        Territory territoryTo = match.getTerritoryTo();
        int minimumAttackingArmies = rulesUtil.getMinimumAttackingArmies();
        int minimumArmiesInTerritories = rulesUtil.getMinimumArmiesInTerritories();
        int minimumArmiesPlacedToPermitAnAttack = minimumAttackingArmies + minimumArmiesInTerritories;

        if(isPlayerConqueredNewTerritory(match)){
            match.deselectTerritories();
            if(territoryFrom.getPlacedArmies() >= minimumArmiesPlacedToPermitAnAttack) {
                match.setAttacker(territoryFrom);
            }
            else if( territoryTo.getPlacedArmies() >= minimumArmiesPlacedToPermitAnAttack) {
                match.setAttacker(territoryTo);
            }
            match.setMovementConfirmed(false);
        }
    }

    @Override
    public void assignArmiesToThePlayer(Match match){
        Player player = match.getPlayerOnDuty();
        Set<Territory> territoriesOwned = match.getTerritoriesOwned(player);
        Set<Continent> continentsOwned = match.getContinentsOwned(player);

        int armiesAvailable = Math.max(
                territoriesOwned.size() / rulesUtil.getNumberOfTerritoryToGetAnArmy(),
                rulesUtil.getMinimumAvailableArmiesForEachTurn()
        );

        for(Continent continent : continentsOwned){
            armiesAvailable += continent.getBonusArmies();
        }

        player.setAvailableArmies(armiesAvailable);

        match.setArmiesWereAssigned(true);
    }

    private Optional<Player> getNextActivePlayerWithAvailableArmies(List<Player> players, Player player){

        Player currentPlayer = player;

        for(int i=0; i<players.size(); i++){
            currentPlayer = getNextActivePlayer(players, currentPlayer).orElse(null);

            if(currentPlayer == null) return Optional.empty();

            if(currentPlayer.getAvailableArmies() > 0){
                return Optional.of(currentPlayer);
            }
        }

        return Optional.empty();
    }

    public Optional<Player> getNextActivePlayer(List<Player> players, Player player){
        for(int i=0; i<players.size(); i++){

            player = getNextPlayer(players, player);

            if(player.isActive()){
                return Optional.of(player);
            }
        }

        return Optional.empty();
    }

    private Player getNextPlayer(List<Player> players, Player player){
        int nextPlayerIndex = players.indexOf(player) + 1;

        if(nextPlayerIndex >= players.size()){
            nextPlayerIndex = 0;
        }

        return players.get(nextPlayerIndex);
    }

    private void setNewPlayerOnDuty(Match match){

        Player newPlayerOnDuty;

        if(match.getActivePlayers().isEmpty()) return;

        if(INITIAL_PLACEMENT.equals(match.getStage())){
            newPlayerOnDuty = getNextActivePlayerWithAvailableArmies(match.getActivePlayers(), match.getPlayerOnDuty())
                    .orElse(null);
        }
        else {
            newPlayerOnDuty = getNextActivePlayer(match.getPlayers(), match.getPlayerOnDuty())
                    .orElse(null);
        }

        if(newPlayerOnDuty != null){
            match.setPlayerOnDuty(newPlayerOnDuty);
        }
    }

    private boolean isPlayerConqueredNewTerritory(Match match){
        Territory territoryFrom = match.getTerritoryFrom();
        Territory territoryTo = match.getTerritoryTo();

        return territoryFrom != null && territoryTo != null && match.isMovementConfirmed();
    }

    private void drawACard(Match match, Player player){
        List<Card> cards = match.getCards();

        if(player.isMustDrawACard() && !cards.isEmpty()){
            player.addCard(cards.getFirst());
            cards.removeFirst();
        }

        player.setMustDrawACard(false);
    }
}
