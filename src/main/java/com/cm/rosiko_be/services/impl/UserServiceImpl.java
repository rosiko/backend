package com.cm.rosiko_be.services.impl;

import com.cm.openapi.model.MatchDto;
import com.cm.openapi.model.UpdateUserRequest;
import com.cm.openapi.model.UserDto;
import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.model.User;
import com.cm.rosiko_be.repositories.UserRepository;
import com.cm.rosiko_be.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

import static com.cm.rosiko_be.mappers.MatchMapper.MATCH_MAPPER;
import static com.cm.rosiko_be.mappers.UserMapper.USER_MAPPER;
import static java.lang.String.format;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService{

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return loadUserByEmail(email);
    }

    @Override
    public User getUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication == null ? null : (User) authentication.getPrincipal();
    }

    @Override
    public UserDto getUserDto() {
        return USER_MAPPER.toUserDto(getUser());
    }

    @Override
    public User loadUserByEmail(String email) throws UsernameNotFoundException {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException(format("User not found with email %s", email)));
    }

    @Override
    public void setCurrentMatch(Match match){
        User user = getUser();
        user.setCurrentMatch(match);
        userRepository.save(user);
    }

    @Override
    public void removeCurrentMatch(User user){
        user.setCurrentMatch(null);
        userRepository.save(user);
    }

    @Override
    public void updateUser(UpdateUserRequest request) {
        User user = getUser();
        boolean isUserChanged = false;

        String newName = request.getName();
        if(newName != null && !user.getName().equals(newName)){
            user.setName(newName);
            isUserChanged = true;
        }

        String newPassword = request.getPassword();
        if(newPassword != null && !user.getPassword().equals(newPassword)){
            user.setPassword(passwordEncoder.encode(request.getPassword()));
            isUserChanged = true;
        }

        if(isUserChanged){
            userRepository.save(user);
        }
    }

    @Override
    public MatchDto getUserCurrentMatch() {
        User user = getUser();
        Match match = user.getCurrentMatch();

        if(match == null) throw new NoSuchElementException("User has no current game");

        return MATCH_MAPPER.toMatchDto(match);
    }
}
