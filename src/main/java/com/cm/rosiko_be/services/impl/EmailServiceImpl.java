package com.cm.rosiko_be.services.impl;

import com.cm.rosiko_be.model.User;
import com.cm.rosiko_be.services.EmailService;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
@RequiredArgsConstructor
@Slf4j
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender emailSender;
    private final TemplateEngine templateEngine;

    @Async("emailTaskExecutor")
    @Override
    public void sendAccountVerificationEmail(User user, String verificationUrl) {
        try {
            MimeMessage mimeMessage = emailSender.createMimeMessage();
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);

            mimeMessageHelper.setTo(user.getEmail());
            mimeMessageHelper.setSubject("Rosiko account verification");

            Context context = new Context();
            context.setVariable("name", user.getName());
            context.setVariable("url", verificationUrl);
            String processedString = templateEngine.process("verificationAccountTemplate", context);

            mimeMessageHelper.setText(processedString, true);

            emailSender.send(mimeMessage);

            log.info("Account verification email has been sent to user " + user.getEmail());
        }
        catch (MessagingException exception){
            log.warn("Error on sending account verification email: ", exception);
        }
    }

    @Override
    @Async("emailTaskExecutor")
    public void sendResetPasswordEmail(User user, String verificationUrl) {
        try {
            MimeMessage mimeMessage = emailSender.createMimeMessage();
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);

            mimeMessageHelper.setTo(user.getEmail());
            mimeMessageHelper.setSubject("Rosiko reset password");

            Context context = new Context();
            context.setVariable("name", user.getName());
            context.setVariable("url", verificationUrl);
            String processedString = templateEngine.process("resetPasswordTemplate", context);

            mimeMessageHelper.setText(processedString, true);

            emailSender.send(mimeMessage);

            log.info("Reset password email has been sent to user " + user.getEmail());
        }
        catch (MessagingException exception){
            log.warn("Error on sending reset password email: ", exception);
        }
    }
}