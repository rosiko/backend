package com.cm.rosiko_be.services;

import com.cm.rosiko_be.model.User;
import jakarta.mail.MessagingException;

public interface EmailService {

    void sendAccountVerificationEmail(User user, String redirectUrl);

    void sendResetPasswordEmail(User user, String redirectUrl);
}
