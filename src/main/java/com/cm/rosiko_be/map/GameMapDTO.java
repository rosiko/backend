package com.cm.rosiko_be.map;

import com.cm.openapi.model.Continent;
import com.cm.openapi.model.TerritoryDto;
import lombok.Data;
import java.util.HashSet;
import java.util.Set;

@Data
public class GameMapDTO {

    private Set<Continent> continents = new HashSet<>();
    private Set<TerritoryDto> territories = new HashSet<>();
}
