package com.cm.rosiko_be.map;

import com.cm.openapi.model.Continent;
import com.cm.rosiko_be.map.territory.Territory;
import lombok.Data;
import java.util.HashSet;
import java.util.Set;

@Data
public class GameMap{
    private Set<Continent> continents;
    private Set<Territory> territories;

    public GameMap(){
        super();
    }

    public Territory getTerritory(String id){
        Territory targetTerritory = null;

        for (Territory territory: territories) {
            if(territory.getId().equals(id)){
                targetTerritory = territory;
                break;
            }
        }
        assert targetTerritory != null;
        return targetTerritory;
    }

    public Set<Territory> getTerritoriesByOwner(String ownerId){
        Set<Territory> playerTerritories = new HashSet<>();

        for (Territory territory: territories) {
            if(ownerId.equals(territory.getOwnerId())){
                playerTerritories.add(territory);
            }
        }

        return playerTerritories;
    }

    public Set<Territory> getNeighbouringTerritories(Territory territory){
        Set<Territory> neighbouringTerritories = new HashSet<>();
        for(Territory mapTerritory : territories){
            if(territory.isBordering(mapTerritory)){
                neighbouringTerritories.add(mapTerritory);
            }
        }
        return neighbouringTerritories;
    }
}
