package com.cm.rosiko_be.map;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static com.cm.rosiko_be.mappers.TerritoryMapper.TERRITORY_MAPPER;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GameMapMapper {

    public static GameMapDTO toGameMapDTO(GameMap gameMap){
        if(gameMap==null) return null;

        GameMapDTO gameMapDTO = new GameMapDTO();
        gameMapDTO.setContinents(gameMap.getContinents());
        gameMapDTO.setTerritories(TERRITORY_MAPPER.toTerritoriesDtos(gameMap.getTerritories()));

        return gameMapDTO;
    }
}
