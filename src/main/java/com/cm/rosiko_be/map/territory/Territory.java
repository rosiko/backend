package com.cm.rosiko_be.map.territory;

import com.cm.openapi.model.CardType;
import com.cm.openapi.model.Color;
import com.cm.rosiko_be.player.Player;
import lombok.Data;
import org.springframework.data.annotation.Id;
import java.util.List;

@Data
public class Territory {
    @Id
    private String id;
    private String name;
    private String continentId;
    private CardType cardType;
    private List<String> neighbouringTerritoriesId;
    private String ownerId;
    private Color color;
    private int placedArmies = 0;
    private boolean isSelectable = false;


    public void setOwner(Player owner) {
        this.ownerId = owner.getId();
        this.color = owner.getColor();
    }

    public void removeArmies(int armies) { this.placedArmies -= armies; }

    public void addArmies(int armies) {
        this.placedArmies += armies;
    }

    public boolean isBordering(Territory territory){
        boolean isBordering = false;
        for(String neighboringID : neighbouringTerritoriesId){
            if(territory.getId().equals(neighboringID)){
                isBordering = true;
                break;
            }
        }
        return isBordering;
    }
}
