package com.cm.rosiko_be.filter;

import com.cm.rosiko_be.model.User;
import com.cm.rosiko_be.services.UserService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import java.io.IOException;

@Component
@Slf4j
@RequiredArgsConstructor
public class LoggingFilter extends OncePerRequestFilter {

    private final UserService userService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        User user = userService.getUser();
        String userId = user != null ? user.getId() : null;
        String currentMatchId = user != null ? user.getCurrentMatchId() : null;
        String requestTemplate = "Request: method={} uri={} user-id={} current-match={}";
        String responseTemplate = "Response: status={} uri={} user-id={} current-match={}";
        String responseErrorTemplate = responseTemplate + " error={}";

        log.info(
                requestTemplate,
                request.getMethod(),
                request.getRequestURI(),
                userId,
                currentMatchId
        );

        try {
            filterChain.doFilter(request, response);
            log.info(responseTemplate, response.getStatus(), request.getRequestURI(), userId, currentMatchId);
        }
        catch (Exception ex){
            log.error(responseErrorTemplate, response.getStatus(), request.getRequestURI(), userId, currentMatchId, ex.getMessage(), ex);
            throw ex;
        }
    }
}
