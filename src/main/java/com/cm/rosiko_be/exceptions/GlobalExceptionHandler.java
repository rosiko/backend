package com.cm.rosiko_be.exceptions;

import com.cm.openapi.model.FaultResponse;
import io.jsonwebtoken.JwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.resource.NoResourceFoundException;

import java.util.*;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(BindException.class)
    public ResponseEntity<FaultResponse> handleValidationExceptions(BindException ex) {
        log.warn(ex.getMessage(), ex);

        StringBuilder errors = new StringBuilder();
        ex.getBindingResult().getAllErrors().forEach( error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            if(errors.isEmpty()){
                errors.append(fieldName).append(" ").append(errorMessage);
            }
            else{
                errors.append(", ").append(fieldName).append(" ").append(errorMessage);
            }
        });

        return ResponseEntity.badRequest()
                .body(new FaultResponse(400, errors.toString()));
    }

    @ExceptionHandler({GameRulesException.class})
    public ResponseEntity<FaultResponse> handleGameRulesExceptions(Exception ex) {
        log.warn(ex.getMessage(), ex);
        return ResponseEntity.badRequest()
                .body(new FaultResponse(400, ex.getMessage()));
    }

    @ExceptionHandler({JwtException.class, AuthenticationException.class})
    public ResponseEntity<FaultResponse> handleAuthenticationExceptions(Exception ex) {
        log.warn(ex.getMessage(), ex);
        return ResponseEntity.status(401)
                .body(new FaultResponse(401, ex.getMessage()));
    }

    @ExceptionHandler({AccountNotVerifiedException.class, RegistrationException.class})
    public ResponseEntity<FaultResponse> handleAccountNotVerifiedExceptions(Exception ex) {
        log.warn(ex.getMessage(), ex);
        return ResponseEntity.status(403)
                .body(new FaultResponse(403, ex.getMessage()));
    }

    @ExceptionHandler({NoSuchElementException.class, NoResourceFoundException.class})
    public ResponseEntity<FaultResponse> handleNotFoundExceptions(Exception ex) {
        log.warn(ex.getMessage(), ex);
        return ResponseEntity.status(404)
                .body(new FaultResponse(404, ex.getMessage()));
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<FaultResponse> handleRemainsExceptions(Exception ex) {
        log.error(ex.getMessage(), ex);
        return ResponseEntity.status(500)
                .body(new FaultResponse(500, ex.getMessage()));
    }
}
