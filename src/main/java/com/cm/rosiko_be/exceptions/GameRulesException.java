package com.cm.rosiko_be.exceptions;

public class GameRulesException extends RuntimeException{
    public GameRulesException(String message, Throwable cause) {
        super(message, cause);
    }

    public GameRulesException(String message) {
        super(message);
    }
}
