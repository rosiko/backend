package com.cm.rosiko_be.exceptions;

import org.springframework.security.authentication.AccountStatusException;

public class AccountNotVerifiedException extends AccountStatusException {
    public AccountNotVerifiedException(String message) {
        super(message);
    }
}
