package com.cm.rosiko_be.mappers;

import com.cm.openapi.model.ArmiesToPlaceDto;
import com.cm.rosiko_be.model.ArmiesToPlace;
import com.cm.rosiko_be.map.GameMap;
import java.util.HashSet;
import java.util.Set;

public class ArmiesToPlaceMapper {

    private ArmiesToPlaceMapper() {
        super();
    }

    public static ArmiesToPlace toArmiesToPlace(ArmiesToPlaceDto armiesToPlaceDto, GameMap map){
        ArmiesToPlace armiesToPlace = new ArmiesToPlace();
        armiesToPlace.setTerritory(map.getTerritory(armiesToPlaceDto.getTerritoryId()));
        armiesToPlace.setQuantity(armiesToPlaceDto.getQuantity());
        return armiesToPlace;
    }

    public static Set<ArmiesToPlace> toArmiesToPlace(Set<ArmiesToPlaceDto> armiesToPlaceDtoSet, GameMap map){
        Set<ArmiesToPlace> armiesToPlaceSet = new HashSet<>();

        for(ArmiesToPlaceDto armiesToPlaceDto : armiesToPlaceDtoSet){
            armiesToPlaceSet.add(toArmiesToPlace(armiesToPlaceDto, map));
        }

        return armiesToPlaceSet;
    }
}
