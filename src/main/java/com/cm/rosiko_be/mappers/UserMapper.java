package com.cm.rosiko_be.mappers;

import com.cm.openapi.model.UserDto;
import com.cm.rosiko_be.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper USER_MAPPER = Mappers.getMapper(UserMapper.class);

    @Mapping( source = "currentMatch.id", target = "currentMatchId")
    UserDto toUserDto(User user);
}
