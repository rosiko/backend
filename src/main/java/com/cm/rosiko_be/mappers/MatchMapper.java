package com.cm.rosiko_be.mappers;

import com.cm.openapi.model.MatchDto;
import com.cm.rosiko_be.match.Match;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = {TerritoryMapper.class, PlayerMapper.class})
public interface MatchMapper {

    MatchMapper MATCH_MAPPER = Mappers.getMapper(MatchMapper.class);

    @Mapping( source = "diceAttacker", target = "attack.dicesAttacker")
    @Mapping( source = "diceDefender", target = "attack.dicesDefender")
    @Mapping( source = "moveArmies", target = "armiesMovement.numberOfArmies")
    @Mapping( source = "attackerId", target = "attack.attackerId")
    @Mapping( source = "defenderId", target = "attack.defenderId")
    @Mapping( source = "territoryFromId", target = "armiesMovement.territoryFromId")
    @Mapping( source = "territoryToId", target = "armiesMovement.territoryToId")
    MatchDto toMatchDto(Match match);

    List<MatchDto> toMatchDto(List<Match> match);
}
