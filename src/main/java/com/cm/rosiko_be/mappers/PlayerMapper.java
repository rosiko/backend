package com.cm.rosiko_be.mappers;

import com.cm.openapi.model.PlayerDto;
import com.cm.rosiko_be.player.Player;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import java.util.List;
import java.util.stream.Collectors;

@Mapper
public interface PlayerMapper {

    PlayerMapper PLAYER_MAPPER = Mappers.getMapper(PlayerMapper.class);

    @Mapping( source = "active", target = "isActive")
    @Mapping( source = "firstTurn", target = "isFirstTurn")
    PlayerDto toPlayerDto(Player player);

    List<PlayerDto> toPlayerDtos(List<Player> players);

    default List<String> mapDefeatedPlayersToDefeatedPlayersId(List<Player> defeatedPlayers){
        return defeatedPlayers.stream()
                .map(Player::getId)
                .toList();
    }
}
