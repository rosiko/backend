package com.cm.rosiko_be.mappers;

import com.cm.openapi.model.TerritoryDto;
import com.cm.rosiko_be.map.territory.Territory;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper
public interface TerritoryMapper {

    TerritoryMapper TERRITORY_MAPPER = Mappers.getMapper(TerritoryMapper.class);

    @Mapping( source = "selectable", target = "isSelectable")
    TerritoryDto toTerritoryDto(Territory territory);

    Set<TerritoryDto> toTerritoriesDtos(Set<Territory> territories);
}
