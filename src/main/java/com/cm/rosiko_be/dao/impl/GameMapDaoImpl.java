package com.cm.rosiko_be.dao.impl;

import com.cm.openapi.model.Continent;
import com.cm.rosiko_be.exceptions.GameRulesException;
import com.cm.rosiko_be.map.GameMap;
import com.cm.rosiko_be.map.territory.Territory;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.util.Set;

@Slf4j
@Component
public class GameMapDaoImpl implements com.cm.rosiko_be.dao.GameMapDao {

    @Value("classpath:data/territories.json")
    Resource territoriesResource;

    @Value("classpath:data/continents.json")
    Resource continentsResource;

    @Override
    public GameMap getGameMap(){
        GameMap gameMap = new GameMap();
        Set<Territory> territories;
        Set<Continent> continents;
        territories = getTerritories();
        continents = getContinents();
        gameMap.setTerritories(territories);
        gameMap.setContinents(continents);

        return gameMap;
    }

    private Set<Continent> getContinents() throws GameRulesException {
        ObjectMapper objectMapper = new ObjectMapper();
        Set<Continent> continents;

        try{
            continents = objectMapper.readValue(continentsResource.getContentAsByteArray(), new TypeReference<>(){});
        } catch (IOException e) {
            throw new GameRulesException("Continents not found",e);
        }

        return continents;
    }

    private Set<Territory> getTerritories() throws GameRulesException {
        ObjectMapper objectMapper = new ObjectMapper();
        Set<Territory> territories;

        try {
            territories = objectMapper.readValue(territoriesResource.getContentAsByteArray(), new TypeReference<>(){});
        } catch (IOException e) {
            throw new GameRulesException("Territories not found",e);
        }

        return territories;
    }
}
