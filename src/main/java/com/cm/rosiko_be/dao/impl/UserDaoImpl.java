package com.cm.rosiko_be.dao.impl;

import com.cm.rosiko_be.dao.UserDao;
import com.cm.rosiko_be.model.User;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Repository
public class UserDaoImpl implements UserDao {

    private final List<User> users = new ArrayList<>();


    @Override
    public void addUser(User user) {
        users.add(user);
    }

    @Override
    public List<User> getUsers() {
        return users;
    }

    @Override
    public User findUserById(String id) {
        if(id!=null){
            for(User user : users){
                if(id.equals(user.getId())) return user;
            }
        }

        throw new NoSuchElementException(String.format("No user found with id %s", id));
    }

    @Override
    public void deleteUser(String id) {
        for(User user : users){
            this.users.remove(user);
        }
    }
}
