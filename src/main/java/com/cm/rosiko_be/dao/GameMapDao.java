package com.cm.rosiko_be.dao;

import com.cm.rosiko_be.exceptions.GameRulesException;
import com.cm.rosiko_be.map.GameMap;

public interface GameMapDao {

    GameMap getGameMap() throws GameRulesException;
}
