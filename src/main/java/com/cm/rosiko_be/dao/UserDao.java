package com.cm.rosiko_be.dao;

import com.cm.rosiko_be.model.User;
import java.util.List;

public interface UserDao {

    void addUser(User user);

    List<User> getUsers();

    User findUserById(String id);

    void deleteUser(String id);
}
