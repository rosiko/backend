package com.cm.rosiko_be.model;

import com.cm.rosiko_be.enums.Role;
import com.cm.rosiko_be.match.Match;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.Collection;
import java.util.List;

@Data
@NoArgsConstructor
@Document
public class User implements UserDetails {
    private String id;
    private String name;
    @Indexed(unique = true)
    private String email;
    private String password;
    @DBRef
    private Match currentMatch;
    private Role role;
    private boolean isActivated;
    private Token temporaryToken;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(role.name()));
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public boolean isActivated() {
        return isActivated;
    }

    public String getCurrentMatchId(){
        return currentMatch!=null? currentMatch.getId() : null;
    }

    public void deleteTemporaryToken(){
        this.temporaryToken = null;
    }
}
