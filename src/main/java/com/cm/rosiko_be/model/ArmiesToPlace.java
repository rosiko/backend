package com.cm.rosiko_be.model;

import com.cm.rosiko_be.map.territory.Territory;
import lombok.Data;

@Data
public class ArmiesToPlace{
    private Territory territory;
    private int quantity;
}
