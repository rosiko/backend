package com.cm.rosiko_be.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Token {
    private String value;
    private LocalDateTime expirationDate;
}
