package com.cm.rosiko_be.statemachine;

import com.cm.openapi.model.MatchState;
import com.cm.rosiko_be.match.Match;

public interface State {

    void updateState(Match match);

    void handle(Match match);
}
