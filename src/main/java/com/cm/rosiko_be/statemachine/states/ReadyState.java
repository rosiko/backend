package com.cm.rosiko_be.statemachine.states;

import com.cm.rosiko_be.enums.MatchEvent;
import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.statemachine.State;
import com.cm.rosiko_be.utils.RulesUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import static com.cm.openapi.model.MatchStage.INITIAL_PLACEMENT;
import static com.cm.openapi.model.MatchState.*;
import static com.cm.rosiko_be.enums.MatchEvent.START_MATCH;

@Component
@RequiredArgsConstructor
public class ReadyState implements State {

    private final RulesUtil rulesUtil;

    @Override
    public void updateState(Match match) {
        int numberOfPlayers = match.getPlayers().size();
        int minimumPlayers = rulesUtil.getMinimumPlayers();
        MatchEvent event = match.getEvent();

        if (START_MATCH.equals(event) && numberOfPlayers >= minimumPlayers) {
            match.setState(STARTED);
            match.setStage(INITIAL_PLACEMENT);
        }
        else if(numberOfPlayers == 0) {
            match.setState(GAME_OVER);
        }
        else if (numberOfPlayers < minimumPlayers) {
            match.setState(WAITING);
        }
        else {
            match.setState(READY);
        }
    }

    @Override
    public void handle(Match match) {}
}
