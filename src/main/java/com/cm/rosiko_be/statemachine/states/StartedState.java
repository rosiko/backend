package com.cm.rosiko_be.statemachine.states;

import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.player.Player;
import com.cm.rosiko_be.statemachine.State;
import com.cm.rosiko_be.statemachine.stages.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static com.cm.openapi.model.MatchStage.CONCLUSION;
import static com.cm.openapi.model.MatchState.GAME_OVER;
import static com.cm.openapi.model.MatchState.STARTED;

@Service
@RequiredArgsConstructor
public class StartedState implements State {

    private final InitialPlacementStage initialPlacementStage;
    private final PlacementStage placementStage;
    private final AttackStage attackStage;
    private final DisplacementStage displacementStage;
    private final ConclusionStage conclusionStage;

    @Override
    public void updateState(Match match) {
        int activePlayers = match.getActivePlayers().size();
        Player winner = match.getWinner();

        if(activePlayers <= 1 || winner != null) {
            match.setState(GAME_OVER);
        }
        else{
            match.setState(STARTED);
        }

        updateStage(match);
    }

    @Override
    public void handle(Match match) {
        State stage = getStage(match);
        stage.handle(match);

        if(CONCLUSION.equals(match.getStage())){
            updateStage(match);
            handle(match);
        }
    }

    private void updateStage(Match match) {
        State stage = getStage(match);
        stage.updateState(match);
    }

    private State getStage(Match match){

        if(match.getStage() == null){
            return getInitialStage();
        }

        return switch (match.getStage()){
            case INITIAL_PLACEMENT -> initialPlacementStage;
            case PLACEMENT -> placementStage;
            case ATTACK -> attackStage;
            case DISPLACEMENT -> displacementStage;
            case CONCLUSION -> conclusionStage;
        };
    }

    private State getInitialStage(){
        return initialPlacementStage;
    }
}
