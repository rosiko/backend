package com.cm.rosiko_be.statemachine.states;

import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.statemachine.State;
import com.cm.rosiko_be.utils.RulesUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import static com.cm.openapi.model.MatchState.*;

@Component
@RequiredArgsConstructor
public class WaitingState implements State {

    private final RulesUtil rulesUtil;

    @Override
    public void updateState(Match match) {
        int numberOfPlayers = match.getPlayers().size();
        int minimumPlayers = rulesUtil.getMinimumPlayers();

        if(numberOfPlayers == 0) {
            match.setState(GAME_OVER);
        }
        else if (numberOfPlayers >= minimumPlayers) {
            match.setState(READY);
        }
        else{
            match.setState(WAITING);
        }
    }

    @Override
    public void handle(Match match) {}
}
