package com.cm.rosiko_be.statemachine.states;

import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.statemachine.State;
import org.springframework.stereotype.Component;

import static com.cm.openapi.model.MatchState.GAME_OVER;

@Component
public class GameOverState implements State {
    @Override
    public void handle(Match match) {}

    @Override
    public void updateState(Match match) {
        match.setState(GAME_OVER);
    }
}
