package com.cm.rosiko_be.statemachine;

import com.cm.openapi.model.MatchState;
import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.statemachine.states.GameOverState;
import com.cm.rosiko_be.statemachine.states.ReadyState;
import com.cm.rosiko_be.statemachine.states.StartedState;
import com.cm.rosiko_be.statemachine.states.WaitingState;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static com.cm.openapi.model.MatchState.*;

@Service
@RequiredArgsConstructor
public class StateMachine {

    private final WaitingState waitingState;
    private final ReadyState readyState;
    private final StartedState startedState;
    private final GameOverState gameOverState;

    public void updateState(Match match){
        State state = getState(match);
        state.updateState(match);
        handle(match);
    }

    private void handle(Match match){
        State state = getState(match);
        state.handle(match);
    }

    private State getState(Match match){

        if(match.getState() == null) return getInitialState();

        return switch (match.getState()){
            case WAITING -> waitingState;
            case READY -> readyState;
            case STARTED -> startedState;
            default -> gameOverState;
        };
    }

    private State getInitialState() {
        return waitingState;
    }
}
