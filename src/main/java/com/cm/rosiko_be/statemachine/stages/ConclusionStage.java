package com.cm.rosiko_be.statemachine.stages;

import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.services.StageService;
import com.cm.rosiko_be.statemachine.State;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import static com.cm.openapi.model.MatchStage.*;

@Component
@RequiredArgsConstructor
public class ConclusionStage implements State {

    private final StageService stageService;

    @Override
    public void updateState(Match match) {

        if(!match.getActivePlayersWithAvailableArmies().isEmpty()){
            match.setStage(INITIAL_PLACEMENT);
        }
        else if(match.isFirstTurnForThePlayers()){
            match.setStage(ATTACK);
        }
        else{
            match.setStage(PLACEMENT);
        }
    }

    @Override
    public void handle(Match match) {
        stageService.endTurn(match);
    }
}
