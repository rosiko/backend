package com.cm.rosiko_be.statemachine.stages;

import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.services.StageService;
import com.cm.rosiko_be.statemachine.State;
import com.cm.rosiko_be.utils.SelectableTerritoriesUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import static com.cm.openapi.model.MatchStage.*;

@Component
@RequiredArgsConstructor
public class InitialPlacementStage implements State {

    private final SelectableTerritoriesUtil selectableTerritories;
    private final StageService stageService;

    @Override
    public void updateState(Match match) {
        match.setStage(CONCLUSION);
    }

    @Override
    public void handle(Match match) {
        stageService.newTurn(match);
        selectableTerritories.update(match);
    }
}
