package com.cm.rosiko_be.statemachine.stages;

import com.cm.rosiko_be.enums.MatchEvent;
import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.player.Player;
import com.cm.rosiko_be.services.StageService;
import com.cm.rosiko_be.statemachine.State;
import com.cm.rosiko_be.utils.SelectableTerritoriesUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import static com.cm.openapi.model.MatchStage.*;
import static com.cm.rosiko_be.enums.MatchEvent.*;

@Component
@RequiredArgsConstructor
public class AttackStage implements State {

    private final StageService stageService;
    private final SelectableTerritoriesUtil selectableTerritories;

    @Override
    public void updateState(Match match) {
        MatchEvent event = match.getEvent();
        Player playerOnDuty = match.getPlayerOnDuty();

        if(END_TURN.equals(event) || !playerOnDuty.isActive()){
            match.setStage(CONCLUSION);
            match.resetEvent();
        }
        else if(END_ATTACKS.equals(event)){
            match.setStage(DISPLACEMENT);
            match.resetEvent();
        }
    }

    @Override
    public void handle(Match match) {
        if(NEW_TURN.equals(match.getEvent())){
            stageService.newTurn(match);
            match.resetEvent();
            match.getPlayerOnDuty().setFirstTurn(false);
        }

        stageService.prepareAttackStage(match);
        selectableTerritories.update(match);
    }
}
