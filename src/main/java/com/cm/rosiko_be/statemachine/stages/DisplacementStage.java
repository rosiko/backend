package com.cm.rosiko_be.statemachine.stages;

import com.cm.rosiko_be.enums.MatchEvent;
import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.statemachine.State;
import com.cm.rosiko_be.utils.SelectableTerritoriesUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import static com.cm.openapi.model.MatchStage.*;
import static com.cm.rosiko_be.enums.MatchEvent.*;

@Component
@RequiredArgsConstructor
public class DisplacementStage implements State {

    private final SelectableTerritoriesUtil selectableTerritories;

    @Override
    public void updateState(Match match) {
        MatchEvent event = match.getEvent();

        if(!match.getPlayerOnDuty().isActive() || ARMIES_DISPLACED.equals(event) || END_TURN.equals(event)){
            match.setStage(CONCLUSION);
            match.resetEvent();
        }
    }

    @Override
    public void handle(Match match) {
        selectableTerritories.update(match);
    }
}
