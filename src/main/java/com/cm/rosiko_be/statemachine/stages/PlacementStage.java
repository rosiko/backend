package com.cm.rosiko_be.statemachine.stages;
import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.player.Player;
import com.cm.rosiko_be.services.StageService;
import com.cm.rosiko_be.statemachine.State;
import com.cm.rosiko_be.utils.SelectableTerritoriesUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import static com.cm.openapi.model.MatchStage.*;
import static com.cm.rosiko_be.enums.MatchEvent.NEW_TURN;

@Component
@RequiredArgsConstructor
public class PlacementStage implements State {

    private final StageService stageService;
    private final SelectableTerritoriesUtil selectableTerritories;

    @Override
    public void updateState(Match match) {
        Player player = match.getPlayerOnDuty();

        if(!player.isActive()){
            match.setStage(CONCLUSION);
        }
        if(player.getAvailableArmies() <= 0){
            match.setStage(ATTACK);
        }
    }

    @Override
    public void handle(Match match) {
        if(NEW_TURN.equals(match.getEvent())){
            stageService.newTurn(match);
            match.resetEvent();
        }

        selectableTerritories.update(match);
    }
}
