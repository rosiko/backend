package com.cm.rosiko_be.repositories;

import com.cm.openapi.model.MatchState;
import com.cm.rosiko_be.match.Match;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;
import java.util.Set;

public interface MatchRepository extends MongoRepository<Match, String> {

    List<Match> findByState(MatchState state);

    List<Match> findByStateIn(Set<MatchState> states);
}
