package com.cm.rosiko_be.utils;

import com.cm.openapi.model.Color;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ColorUtil {

    private static final Random random = new Random();


    public static Color getRandomColor(List<Color> excludedColors){
        List<Color> listOfAcceptableColors = getListOfRemainingColors(excludedColors);

        if(listOfAcceptableColors.isEmpty()) throw new NoSuchElementException("There are no more colors available");
        int index = random.nextInt(listOfAcceptableColors.size());

        return listOfAcceptableColors.get(index);
    }

    private static List<Color> getListOfRemainingColors(List<Color> excludedColors){
        List<Color> remainingColors = new ArrayList<>();

        for (Color color : Color.values()) {
            if(!excludedColors.contains(color)) remainingColors.add(color);
        }

        return remainingColors;
    }
}
