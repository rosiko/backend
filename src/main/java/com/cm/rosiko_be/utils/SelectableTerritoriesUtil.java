package com.cm.rosiko_be.utils;

import com.cm.rosiko_be.map.territory.Territory;
import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.player.Player;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class SelectableTerritoriesUtil {

    private final RulesUtil rulesUtil;


    public void update(Match match) {
        switch (match.getStage()) {
            case INITIAL_PLACEMENT, PLACEMENT -> setSelectableTerritoriesForPlacementPhase(match);
            case ATTACK -> setSelectableTerritoriesForAttackPhase(match);
            case DISPLACEMENT -> setSelectableTerritoriesForDisplacementPhase(match);
            default -> throw new IllegalStateException("Unexpected value: " + match.getStage());
        }
    }


    private void setSelectableTerritoriesForPlacementPhase(Match match){
        Player playerOnDuty = match.getPlayerOnDuty();

        for(Territory territory : match.getMap().getTerritories()){
            territory.setSelectable( playerOnDuty.isTheOwner(territory) );
        }
    }

    private void setSelectableTerritoriesForAttackPhase(Match match){
        Set<Territory> territories = match.getMap().getTerritories();

        if(isTerritoryConquered(match)) {
            for(Territory territory : territories){
                territory.setSelectable( rulesUtil.isTerritoryAbleToAttack(match, territory) );
            }
        }

        if(isPlayerAbleToSelectAnAttackerTerritory(match)){
            for(Territory territory : territories){
                territory.setSelectable( rulesUtil.isTerritoryAbleToMoveArmiesFrom(match, territory) );
            }
        }

        if(isPlayerAbleToSelectATerritoryToAttack(match)){
            for(Territory territory : territories){
                territory.setSelectable(
                        rulesUtil.isTerritoryAbleToAttack(match, territory)
                        || rulesUtil.isTerritoryAttackable(match, territory)
                );
            }
        }
    }

    private void setSelectableTerritoriesForDisplacementPhase(Match match){

        if(isPlayerAbleToSelectATerritoryFrom(match)){
            setSelectableTerritoriesToBeMovedFrom(match);
        }

        if(isPlayerAbleToSelectATerritoryTo(match)){
            setSelectableTerritoriesToBeMovedTo(match);
        }
    }

    private void setSelectableTerritoriesToBeMovedFrom(Match match){
        Set<Territory> territories = match.getMap().getTerritories();

        for(Territory territory : territories){
            territory.setSelectable( rulesUtil.isTerritoryAbleToMoveArmiesFrom(match, territory));
        }
    }

    private void setSelectableTerritoriesToBeMovedTo(Match match){
        for(Territory territory : match.getMap().getTerritories()){
            territory.setSelectable( rulesUtil.isTerritoryAbleToReceiveArmiesFrom(match, territory));
        }
    }


    private boolean isTerritoryConquered(Match match){
        Territory territoryFrom = match.getTerritoryFrom();
        Territory territoryTo = match.getTerritoryTo();

        return  territoryFrom != null
                && territoryTo != null
                && territoryFrom.getOwnerId().equals(territoryTo.getOwnerId());
    }

    private boolean isPlayerAbleToSelectAnAttackerTerritory(Match match){
        return match.getAttacker() == null;
    }

    private boolean isPlayerAbleToSelectATerritoryToAttack(Match match){
        return match.getAttacker() != null;
    }

    private boolean isPlayerAbleToSelectATerritoryFrom(Match match){
        return match.getTerritoryFrom() == null;
    }

    private boolean isPlayerAbleToSelectATerritoryTo(Match match){
        return match.getTerritoryFrom() != null;
    }
}
