package com.cm.rosiko_be.utils;

import com.cm.rosiko_be.match.Match;

@FunctionalInterface
public interface ExpiredTimerFunction {

    void apply(Match match);
}
