package com.cm.rosiko_be.utils;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class TimeUtil {

    public LocalDateTime now(){
        return LocalDateTime.now();
    }
}
