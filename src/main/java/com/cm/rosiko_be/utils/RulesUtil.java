package com.cm.rosiko_be.utils;

import com.cm.rosiko_be.map.GameMap;
import com.cm.rosiko_be.map.territory.Territory;
import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.player.Player;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@Getter
@PropertySource("classpath:game-rules.properties")
public class RulesUtil {

    @Value("${minimum-players}")
    private int minimumPlayers;
    @Value("${maximum-players}")
    private int maximumPlayers;
    @Value("${number-of-jolly}")
    private int numberOfJolly;
    @Value("${minimum-armies-in-territories}")
    private int minimumArmiesInTerritories;
    @Value("${tractor-set-bonus}")
    private int tractorSetBonus;
    @Value("${farmer-set-bonus}")
    private int farmerSetBonus;
    @Value("${cow-set-bonus}")
    private int cowSetBonus;
    @Value("${different-cards-set-bonus}")
    private int differentCardsSetBonus;
    @Value("${jolly-set-bonus}")
    private int jollySetBonus;
    @Value("${territory-owner-bonus}")
    private int territoryOwnerBonus;
    @Value("${number-of-territory-to-get-an-army}")
    private int numberOfTerritoryToGetAnArmy;
    @Value("${minimum-available-armies-for-each-turn}")
    private int minimumAvailableArmiesForEachTurn;
    @Value("${minimum-attacking-armies}")
    private int minimumAttackingArmies;

    public boolean isTerritoryAbleToAttack(Match match, Territory territory){
        Player playerOnDuty = match.getPlayerOnDuty();
        GameMap map = match.getMap();
        boolean isTerritoryOwnerOnDuty = playerOnDuty.isTheOwner(territory);
        boolean hasEnoughArmies = territory.getPlacedArmies() > minimumArmiesInTerritories;
        boolean isTerritoryNotSelected = !territory.equals(match.getAttacker());
        boolean hasBorderingEnemy = false;


        for(Territory neighbouring : map.getNeighbouringTerritories(territory)){
            if(!playerOnDuty.isTheOwner(neighbouring)){
                hasBorderingEnemy = true;
                break;
            }
        }

        return isTerritoryOwnerOnDuty && hasEnoughArmies && hasBorderingEnemy && isTerritoryNotSelected;
    }

    public boolean isTerritoryAttackable(Match match, Territory defender){
        Territory attacker = match.getAttacker();
        boolean isDefenderNearTheAttacker = attacker.isBordering(defender);
        boolean isDefenderNotOwnedByAttackerPlayer = !attacker.getOwnerId().equals(defender.getOwnerId());

        return isDefenderNearTheAttacker && isDefenderNotOwnedByAttackerPlayer;
    }

    boolean isTerritoryAbleToMoveArmiesFrom(Match match, Territory territory){
        Player playerOnDuty = match.getPlayerOnDuty();
        return playerOnDuty.isTheOwner(territory) && territory.getPlacedArmies() > minimumArmiesInTerritories;
    }

    boolean isTerritoryAbleToReceiveArmiesFrom(Match match, Territory territory){
        Player playerOnDuty = match.getPlayerOnDuty();
        Territory territoryFrom = match.getTerritoryFrom();

        boolean isNotTerritoryFrom = !territory.equals(territoryFrom);
        boolean isPlayerOnDutyTheOwner = playerOnDuty.isTheOwner(territory);
        boolean isTerritoryBordering = territoryFrom.isBordering(territory);

        return isNotTerritoryFrom && isPlayerOnDutyTheOwner && isTerritoryBordering;
    }
}
