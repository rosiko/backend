package com.cm.rosiko_be.utils;

import com.cm.openapi.model.Card;
import com.cm.openapi.model.CardType;
import com.cm.rosiko_be.map.GameMap;
import com.cm.rosiko_be.map.territory.Territory;
import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.player.Player;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import java.util.HashSet;
import java.util.Set;

import static com.cm.openapi.model.CardType.*;

@Component
@RequiredArgsConstructor
public class CardsUtil {

    private final RulesUtil rulesUtil;

    public int getArmiesBonus(Match match, Set<Card> cards){
        int setBonus = 0;
        int territoryOwnedBonus = 0;

        if(isSameTypeSet(cards, COW)) setBonus += rulesUtil.getCowSetBonus();
        else if(isSameTypeSet(cards, TRACTOR)) setBonus += rulesUtil.getTractorSetBonus();
        else if(isSameTypeSet(cards, FARMER)) setBonus += rulesUtil.getFarmerSetBonus();
        else if(isDifferentTypeSet(cards)) setBonus += rulesUtil.getDifferentCardsSetBonus();
        else if(isJollySet(cards)) setBonus += rulesUtil.getJollySetBonus();

        if(setBonus > 0){
            territoryOwnedBonus = getTerritoryOwnedBonus(match, cards);
        }

        return setBonus + territoryOwnedBonus;
    }

    private int getTerritoryOwnedBonus(Match match, Set<Card> cards){
        int territoryOwnedBonus = 0;
        Player player = match.getPlayerOnDuty();
        GameMap map = match.getMap();

        for(Card card : cards){
            Territory territory = map.getTerritory(card.getTerritoryId());
            if(player.isTheOwner(territory)) territoryOwnedBonus += rulesUtil.getTerritoryOwnerBonus();
        }

        return territoryOwnedBonus;
    }

    private boolean isSameTypeSet(Set<Card> cards, CardType cardType){
        boolean isSameTypeSet = true;

        for(Card card : cards){
            if(!card.getCardType().equals(cardType)){
                isSameTypeSet = false;
                break;
            }
        }

        return isSameTypeSet;
    }

    private boolean isDifferentTypeSet(Set<Card> cards){
        boolean isDifferentTypeSet = true;
        Set<CardType> cardTypes = new HashSet<>();

        for(Card card : cards){
            if(cardTypes.contains(card.getCardType())){
                isDifferentTypeSet = false;
                break;
            }
            else{
                cardTypes.add(card.getCardType());
            }
        }

        return isDifferentTypeSet;
    }

    private boolean isJollySet(Set<Card> cards){
        int jollyCounter = 0;
        Set<CardType> notJollyCards = new HashSet<>();

        for(Card card : cards){
            if(card.getCardType().equals(JOLLY)) jollyCounter++;
            else notJollyCards.add(card.getCardType());
        }

        return jollyCounter == 1 && notJollyCards.size() == 1;
    }
}
