package com.cm.rosiko_be.player;

import com.cm.openapi.model.Card;
import com.cm.openapi.model.Color;
import com.cm.rosiko_be.map.territory.Territory;
import com.cm.rosiko_be.mission.Mission;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.*;

@Document
@Data
public class Player {
    @Id
    private String id;
    private String name;
    private Color color;
    private int availableArmies = 0;                //Armate che ha a disposizione il giocatore per essere posizionate sui territori
    private Mission mission;                        //Obiettivo da raggiungere per vincere la partita
    private int armiesPlacedThisTurn = 0;           //Armate che il giocatore ha piazzato durante il turno
    private boolean isActive;
    private Set<String> defeatedPlayersId = new HashSet<>();
    private boolean mustDrawACard = false;          //Se a fine turno il giocatore deve pescare una carta
    private List<Card> cards = new ArrayList<>();   //Lista di carte a disposizione del giocatore
    private boolean isFirstTurn = true;

    public Player(){
        super();
    }

    public Player(String id, String name){
        this.id = id;
        this.name = name;
        this.isActive = true;
    }

    public void increaseArmiesPlacedThisTurn(int increase) {
        this.armiesPlacedThisTurn += increase;
    }

    public void decreaseAvailableArmies(int quantity) {
        this.availableArmies -= quantity;
    }

    public void addDefeatedPlayer(Player player){
        defeatedPlayersId .add(player.getId());
    }

    public void setDefeatedPlayers(Set<Player> players){
        for(Player player : players){
            addDefeatedPlayer(player);
        }
    }

    public void addCard(Card card) {
        cards.add(card);
    }

    public void addCards(List<Card> cards) {
        this.cards.addAll(cards);
    }

    public void removeCard(Card card) {
        cards.remove(card);
    }

    public List<Card> takeCards(){
        List<Card> takenCards = this.cards;
        this.cards = new ArrayList<>();
        return takenCards;
    }

    public Card getCard(long cardId){
        for(Card card : cards){
            if(card.getId().equals(cardId)){
                return card;
            }
        }

        throw new NoSuchElementException(String.format("Player %s has no card wit id %s", id, cardId));
    }

    public boolean isTheOwner(Territory territory){
        return id.equals(territory.getOwnerId());
    }

    public void resetActions(){
        armiesPlacedThisTurn=0;
        mustDrawACard=false;
    }
}
