package com.cm.rosiko_be.socket;

import com.cm.openapi.model.MatchDto;
import com.cm.rosiko_be.services.impl.MatchServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

import static com.cm.openapi.model.MatchState.READY;
import static com.cm.openapi.model.MatchState.WAITING;

@Slf4j
@Service
@RequiredArgsConstructor
public class WebSocketService {

    private final SimpMessagingTemplate messagingTemplate;
    private final MatchServiceImpl matchService;

    public void notifyAvailableMatches(){
        List<MatchDto> availableMatches = matchService.getMatches(Set.of(WAITING, READY));
        messagingTemplate.convertAndSend("/topic/available-matches", availableMatches);
    }

    public void notifyUpdatedMatch(String matchId){
        MatchDto matchDto = matchService.getMatch(matchId);
        String topic = "/topic/matches/" + matchId;
        messagingTemplate.convertAndSend(topic, matchDto);
    }
}
