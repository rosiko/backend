package com.cm.rosiko_be.mission;

import com.cm.openapi.model.Continent;
import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.player.Player;

import java.util.List;
import java.util.Set;

public class Mission05 extends Mission{

    public Mission05(long id){
        super(id);
        description = "Capture North America and Oceania.";
    }

    @Override
    public boolean isMissionCompleted(Player player, Match match) {

        boolean northAmerica = false;
        boolean oceania = false;

        //Lista dei continenti posseduti dal giocatore
        Set<Continent> continents = match.getContinentsOwned(player);

        for (Continent continent : continents) {
            //Controlla che abbia preso il Nord America
            if(continent.getId().equals("north_america")) northAmerica = true;
            //Controlla che abbia preso l'Oceania
            if(continent.getId().equals("oceania")) oceania = true;
        }

        return northAmerica && oceania;
    }
}
