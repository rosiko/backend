package com.cm.rosiko_be.mission;

import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.player.Player;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public abstract class Mission{

    protected long id;
    protected String description;

    protected Mission(){
        super();
    }

    protected Mission(long id){
        this.id = id;
    }

    public abstract boolean isMissionCompleted(Player player, Match match);
}
