package com.cm.rosiko_be.mission;

import com.cm.openapi.model.Continent;
import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.player.Player;

import java.util.List;
import java.util.Set;

public class Mission01 extends Mission{

    public Mission01(long id){
        super( id);
        description = "Capture Europe, Oceania and one other continent.";
    }

    @Override
    public boolean isMissionCompleted(Player player, Match match) {

        boolean oceania = false;
        boolean europe = false;
        boolean thirdContinent = false;

        //Lista dei continenti posseduti dal giocatore
        Set<Continent> continents = match.getContinentsOwned(player);

        for (Continent continent : continents) {
            //Controlla che abbia preso l'oceania
            if(continent.getId().equals("oceania")) oceania = true;
            //Controlla che abbia preso l'europa
            if(continent.getId().equals("europe")) europe = true;
            //Controlla che abbia preso un terzo continente
            if(!continent.getId().equals("oceania") && !continent.getId().equals("europe")) thirdContinent = true;
        }

        return oceania && europe && thirdContinent;
    }
}
