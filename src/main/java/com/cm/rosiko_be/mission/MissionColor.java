package com.cm.rosiko_be.mission;

import com.cm.openapi.model.Color;
import com.cm.rosiko_be.map.territory.Territory;
import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.player.Player;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MissionColor extends Mission{

    private Color enemyColor;

    private String targetId = null;

    private boolean conquest24Territories = false;

    public MissionColor(long id, Color color){
        super(id);
        enemyColor = color;
        description = "Destroy all armies of " + color.toString().toLowerCase() + " or, in the case of being the named player oneself, to capture 24 territories.";
    }

    @Override
    public boolean isMissionCompleted(Player player, Match match) {
        if(player==null) return false;
        List<Player> players = match.getPlayers();
        boolean completedMission = false;

        //Controlla se il giocatore con quel colore è presente e non è se stesso.
        if(!conquest24Territories && targetId == null){
            Player target = match.findPlayerByColor(enemyColor);
            conquest24Territories = target == null || target.equals(player);
            if(!conquest24Territories) targetId = target.getId();
        }

        //Controlla se il target esista ed è stato sconfitto.
        if(!conquest24Territories){
            for(Player currentPlayer : players){
                Set<String> defeatedPlayersIds = currentPlayer.getDefeatedPlayersId();
                if(defeatedPlayersIds.contains(targetId)){
                    completedMission = player.equals(currentPlayer);
                    conquest24Territories = !completedMission;
                    break;
                }
            }
        }

        //Se l'obiettivo è diventato la conquista dei territori, controlla che sia stato raggiunto.
        else{
            Set<Territory> territories = match.getTerritoriesOwned(player);
            completedMission = territories.size() >= 24;
        }

        return completedMission;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MissionColor that = (MissionColor) o;
        return conquest24Territories == that.conquest24Territories && enemyColor == that.enemyColor && Objects.equals(targetId, that.targetId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), enemyColor, targetId, conquest24Territories);
    }
}
