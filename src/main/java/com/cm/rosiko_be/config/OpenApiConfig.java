package com.cm.rosiko_be.config;

import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.ArrayList;
import java.util.List;

@Configuration
@SecurityScheme(
        name = "bearerAuth",
        type = SecuritySchemeType.HTTP,
        bearerFormat = "JWT",
        scheme = "bearer"
)
public class OpenApiConfig {

        @Value("${local_url}")
        private String localUrl;

        @Value("${prod_url}")
        private String prodUrl;

        @Bean
        public OpenAPI customOpenAPI(){
                OpenAPI openAPI = new OpenAPI();
                openAPI.info(getInfo());
                openAPI.setServers(getServers());
                return openAPI;
        }

        private List<Server> getServers(){
                List<Server> servers = new ArrayList<>();

                Server local = new Server();
                local.setDescription("Local");
                local.setUrl(localUrl);
                servers.add(local);

                Server prod = new Server();
                prod.setDescription("Production");
                prod.setUrl(prodUrl);
                servers.add(prod);

                return servers;
        }

        private Info getInfo(){
                Info info = new Info();
                info.setTitle("Rosiko");
                info.setVersion("v1");
                return info;
        }
}
