package com.cm.rosiko_be.enums;

public enum Role {
    USER,
    ADMIN
}
