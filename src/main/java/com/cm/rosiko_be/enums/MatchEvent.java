package com.cm.rosiko_be.enums;

public enum MatchEvent {
    START_MATCH,
    ARMIES_DISPLACED,
    END_ATTACKS,
    END_TURN,
    PLAY_CARDS,
    NEW_TURN
}
