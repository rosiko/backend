package com.cm.rosiko_be.dao;

import com.cm.openapi.model.Continent;
import com.cm.rosiko_be.map.GameMap;
import com.cm.rosiko_be.map.territory.Territory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class GameMapDaoTest {

    @Autowired
    private GameMapDao gameMapDao;

    @Test
    void getGameMapTest(){

        GameMap gameMap = gameMapDao.getGameMap();
        Set<Territory> territories = gameMap.getTerritories();
        Set<Continent> continents = gameMap.getContinents();

        assertNotNull(territories);
        assertNotNull(continents);
        assertFalse(territories.isEmpty());
        assertFalse(continents.isEmpty());
    }
}
