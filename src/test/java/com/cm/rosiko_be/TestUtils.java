package com.cm.rosiko_be;

import com.cm.openapi.model.*;
import com.cm.rosiko_be.map.GameMap;
import com.cm.rosiko_be.map.territory.Territory;
import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.mission.Mission;
import com.cm.rosiko_be.mission.Mission01;
import com.cm.rosiko_be.player.Player;

import java.util.*;
import static com.cm.openapi.model.CardType.*;
import static com.cm.openapi.model.Color.RED;
import static com.cm.openapi.model.MatchStage.ATTACK;
import static com.cm.openapi.model.MatchState.READY;
import static com.cm.openapi.model.MatchState.STARTED;


public class TestUtils {

    public static List<Match> getMatches(int quantity){
        List<Match> matches = new ArrayList<>();

        for(int i=1; i<=quantity; i++){
            matches.add(getMatch(i+""));
        }

        return matches;
    }

    public static Match getMatch(String id){
        Match match = getMatch();
        match.setId(id);
        match.setName("match_" + id);
        return match;
    }

    public static Match getMatch(String id, int players){
        Match match = getMatch(id);
        match.setPlayers(getPlayers(players));
        return match;
    }

    public static Match getMatch(){
        Match match = new Match();
        List<Player> players = getPlayers(3);
        GameMap map = getGameMap(2);
        List<Integer> attackDices = new ArrayList<>();
        attackDices.add(1);
        attackDices.add(6);
        List<Integer> defenseDices = new ArrayList<>();
        defenseDices.add(2);
        defenseDices.add(5);
        defenseDices.add(1);

        match.setId("1");
        match.setName("Partitone");
        match.setPassword("123");
        match.setState(STARTED);
        match.setPlayers(players);
        match.setMap(map);
        match.setPlayerOnDuty(players.getFirst());
        match.setWinner(players.getLast());
        match.setTurn(10);
        match.setStage(ATTACK);
        match.setAttacker(map.getTerritory("1"));
        match.setDefender(map.getTerritory("2"));
        match.setDiceAttacker(attackDices);
        match.setDiceDefender(defenseDices);
        match.setTerritoryFrom(map.getTerritory("1"));
        match.setTerritoryTo(map.getTerritory("2"));
        match.setMoveArmies(3);
        match.setMovementConfirmed(true);
        match.setArmiesWereAssigned(false);
        setDeckOfCards(match);

        return match;
    }



    public static MatchDto getMatchDto(){
        MatchDto matchDto = new MatchDto();
        List<PlayerDto> players = getPlayersDto();
        GameMapDto map = getGameMapDto();

        AttackDto attackDto = new AttackDto();
        attackDto.setAttackerId("1");
        attackDto.setDefenderId("2");
        attackDto.setDicesAttacker(new ArrayList<>() {{ add(1); add(6); }});
        attackDto.setDicesDefender(new ArrayList<>() {{ add(2); add(5); add(1); }});

        ArmiesMovement armiesMovement = new ArmiesMovement();
        armiesMovement.setTerritoryFromId("1");
        armiesMovement.setTerritoryToId("2");
        armiesMovement.setNumberOfArmies(3);

        matchDto.setId("1");
        matchDto.setName("Partitone");
        matchDto.setPassword("123");
        matchDto.setState(STARTED);
        matchDto.setPlayers(players);
        matchDto.setMap(map);
        matchDto.setPlayerOnDutyId(players.getFirst().getId());
        matchDto.setWinnerId(players.getLast().getId());
        matchDto.setTurn(10);
        matchDto.setStage(ATTACK);
        matchDto.setAttack(attackDto);
        matchDto.setArmiesMovement(armiesMovement);
        matchDto.setMovementConfirmed(true);
        matchDto.setArmiesWereAssigned(false);
        setDeckOfCards(matchDto);

        return matchDto;
    }

    public static Player getPlayer(String id){
        Player player = new Player();
        player.setId(id);
        return player;
    }

    private static List<Player> getPlayers(int numberOfPlayers){
        List<Player> players = new ArrayList<>();

        for(int i=1; i<=numberOfPlayers; i++){
            Player player = new Player(""+i, "Player"+i);
            players.add(player);
        }

        return players;
    }

    private static List<PlayerDto> getPlayersDto(){
        List<PlayerDto> players = new ArrayList<>();

        PlayerDto player1 = new PlayerDto();
        player1.setId("1");
        player1.setName("Player1");
        player1.setAvailableArmies(0);
        player1.setArmiesPlacedThisTurn(0);
        player1.setMustDrawACard(false);
        player1.isActive(true);

        PlayerDto player2 = new PlayerDto();
        player2.setId("2");
        player2.setName("Player2");
        player2.setAvailableArmies(0);
        player2.setArmiesPlacedThisTurn(0);
        player2.setMustDrawACard(false);
        player2.isActive(true);

        PlayerDto player3 = new PlayerDto();
        player3.setId("3");
        player3.setName("Player3");
        player3.setAvailableArmies(0);
        player3.setArmiesPlacedThisTurn(0);
        player3.setMustDrawACard(false);
        player3.isActive(true);

        players.add(player1);
        players.add(player2);
        players.add(player3);

        return players;
    }

    public static GameMap getGameMap(int numberOfTerritories){
        GameMap map = new GameMap();
        Player owner = getPlayers(1).getFirst();

        Continent continent = new Continent();
        continent.setId("1");
        continent.setName("continent_1");
        continent.setBonusArmies(5);
        Set<Continent> continents = new HashSet<>(){{ add(continent); }};
        Set<Territory> territories = new HashSet<>();

        for(int i=1; i<=numberOfTerritories; i++){
            Territory territory = new Territory();
            territory.setId(""+i);
            territory.setName("territory_"+i);
            territory.setContinentId("continent_1");
            territory.setCardType(TRACTOR);

            int index = i+1;
            territory.setNeighbouringTerritoriesId(new ArrayList<>() {{ add("territory_" + index); }});
            territory.setOwner(owner);
            territory.setColor(RED);
            territory.setPlacedArmies(0);
            territory.setSelectable(true);

            territories.add(territory);
            continent.getTerritoriesId().add(territory.getId());
        }

        map.setTerritories(territories);
        map.setContinents(continents);

        return map;
    }

    public static GameMapDto getGameMapDto(){
        GameMapDto map = new GameMapDto();
        Player owner = getPlayers(1).getFirst();

        TerritoryDto territory1 = new TerritoryDto();
        territory1.setId("1");
        territory1.setName("territory_1");
        territory1.setContinentId("continent_1");
        territory1.setCardType(TRACTOR);
        territory1.setNeighbouringTerritoriesId(new ArrayList<>() {{ add("territory_2"); }});
        territory1.setOwnerId(owner.getId());
        territory1.setColor(RED);
        territory1.setPlacedArmies(0);
        territory1.setIsSelectable(true);
        territory1.setOwnerId("1");

        TerritoryDto territory2 = new TerritoryDto();
        territory2.setId("2");
        territory2.setName("territory_2");
        territory2.setContinentId("continent_1");
        territory2.setCardType(TRACTOR);
        territory2.setNeighbouringTerritoriesId(new ArrayList<>() {{ add("territory_3"); }});
        territory2.setColor(RED);
        territory2.setPlacedArmies(0);
        territory2.setIsSelectable(true);
        territory2.setOwnerId("1");


        Continent continent1 = new Continent();
        continent1.setId("1");
        continent1.setName("continent_1");
        continent1.setBonusArmies(5);
        continent1.setTerritoriesId(new ArrayList<>(){{ add("1"); add("2"); }});

        Set<Continent> continents = new HashSet<>(){{ add(continent1); }};


        map.setTerritories(new HashSet<>(){{ add(territory1); add(territory2); }});
        map.setContinents(continents);
        return map;
    }

    private static void setDeckOfCards(Match match){
        List<Card> cards = new ArrayList<>();

        Card card = new Card();
        card.setId(1L);
        card.setTerritoryId("1");
        card.setTerritoryName("Italy");
        card.setCardType(COW);
        card.setSelected(false);
        cards.add(card);

        Card jolly = new Card();
        jolly.setId(2L);
        jolly.setCardType(JOLLY);
        jolly.setSelected(false);
        cards.add(jolly);

        match.setCards(cards);
    }

    private static void setDeckOfCards(MatchDto match){
        Set<Card> cards = new HashSet<>();

        Card card = new Card();
        card.setId(1L);
        card.setTerritoryId("1");
        card.setTerritoryName("Italy");
        card.setCardType(COW);
        card.setSelected(false);
        cards.add(card);

        Card jolly = new Card();
        jolly.setId(2L);
        jolly.setCardType(JOLLY);
        jolly.setSelected(false);
        cards.add(jolly);

        match.setCards(cards);
    }

    public static List<Mission> getMissions(int quantity){

        List<Mission> missions = new ArrayList<>();

        for(int i=0; i<quantity; i++){
            missions.add(new Mission01(0));
        }

        return missions;
    }

    public static Match getReadyMatch(String matchId, int numberOfPlayers, int numberOfTerritories){
        Match match = new Match("ready_match");
        match.setId(matchId);
        match.setState(READY);
        match.setPlayers(getPlayers(numberOfPlayers));
        match.setMap(getGameMap(numberOfTerritories));
        return match;
    }

    public static Match getStartedMatch(String matchId,  int numberOfPlayers, int numberOfTerritories, MatchStage stage){
        Match match = new Match("started_match");
        match.setId(matchId);
        match.setState(STARTED);
        match.setPlayers(getPlayers(numberOfPlayers));
        match.setStage(stage);
        match.setMap(getGameMap(numberOfTerritories));
        return match;
    }
}
