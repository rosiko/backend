package com.cm.rosiko_be.mappers;

import com.cm.openapi.model.ArmiesToPlaceDto;
import com.cm.rosiko_be.TestUtils;
import com.cm.rosiko_be.model.ArmiesToPlace;
import com.cm.rosiko_be.map.GameMap;
import com.cm.rosiko_be.map.territory.Territory;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.cm.rosiko_be.mappers.ArmiesToPlaceMapper.toArmiesToPlace;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;

@SpringBootTest
class ArmiesToPlaceMapperTest {

    @Test
    void testArmiesToPlace(){

        GameMap map = TestUtils.getGameMap(2);
        List<Territory> territories = new ArrayList<>(map.getTerritories());
        Territory territory = territories.getFirst();

        ArmiesToPlaceDto armiesToPlaceDto = new ArmiesToPlaceDto();
        armiesToPlaceDto.setTerritoryId(territory.getId());
        armiesToPlaceDto.setQuantity(3);
        Set<ArmiesToPlaceDto> armiesToPlaceDtoSet = new HashSet<>();
        armiesToPlaceDtoSet.add(armiesToPlaceDto);

        ArmiesToPlace armiesToPlace = new ArmiesToPlace();
        armiesToPlace.setTerritory(territory);
        armiesToPlace.setQuantity(3);
        Set<ArmiesToPlace> expected = new HashSet<>();
        expected.add(armiesToPlace);

        assertIterableEquals(expected, toArmiesToPlace(armiesToPlaceDtoSet, map));
    }
}
