package com.cm.rosiko_be.mappers;

import com.cm.openapi.model.TerritoryDto;
import com.cm.rosiko_be.map.territory.Territory;
import com.cm.rosiko_be.mission.Mission01;
import com.cm.rosiko_be.player.Player;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static com.cm.openapi.model.Color.RED;
import static com.cm.rosiko_be.mappers.TerritoryMapper.TERRITORY_MAPPER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest
class TerritoryMapperTest {

    private static List<Territory> territories;


    @BeforeAll
    static void initializeTerritories(){
        try {
            territories = getTerritories();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void toTerritoryDtoTest(){
        Territory territory = territories.getFirst();
        territory.setOwner( getPlayer());

        TerritoryDto expected = new TerritoryDto();
        expected.setId(territory.getId());
        expected.setName(territory.getName());
        expected.setContinentId(territory.getContinentId());
        expected.setCardType(territory.getCardType());
        expected.setNeighbouringTerritoriesId(territory.getNeighbouringTerritoriesId());
        expected.setOwnerId(territory.getOwnerId());
        expected.setColor(territory.getColor());
        expected.setPlacedArmies(territory.getPlacedArmies());
        expected.setIsSelectable(territory.isSelectable());

        assertEquals(expected, TERRITORY_MAPPER.toTerritoryDto(territory));
    }

    @Test
    void toTerritoriesDtosTest(){
        assertFalse(TERRITORY_MAPPER.toTerritoriesDtos(new HashSet<>(territories)).isEmpty());
    }


    private static List<Territory> getTerritories() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        File file = new File("src/test/resources/data/territories.json");
        return objectMapper.readValue(file, new TypeReference<>(){});
    }

    private Player getPlayer(){
        Player player = new Player("4", "Player4");
        player.setColor(RED);
        player.setAvailableArmies(0);
        player.setMission(new Mission01(1L));
        player.setArmiesPlacedThisTurn(0);
        player.setMustDrawACard(false);
        player.setActive(true);
        return player;
    }

    private List<String> getTerritoriesId(List<Territory> territories){
        List<String> territoriesId = new ArrayList<>();

        for (Territory territory : territories){
            territoriesId.add(territory.getId());
        }

        return territoriesId;
    }
}
