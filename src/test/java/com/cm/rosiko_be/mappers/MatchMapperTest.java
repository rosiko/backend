package com.cm.rosiko_be.mappers;

import com.cm.openapi.model.MatchDto;
import com.cm.rosiko_be.TestUtils;
import com.cm.rosiko_be.match.Match;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.ArrayList;
import java.util.List;

import static com.cm.rosiko_be.mappers.MatchMapper.MATCH_MAPPER;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class MatchMapperTest {

    @Test
    void testMatchToMatchDto(){

        Match match = TestUtils.getMatch();
        MatchDto expected = TestUtils.getMatchDto();

        assertEquals( expected, MATCH_MAPPER.toMatchDto(match) );

    }

    @Test
    void testMatchToMatchDtos(){

        List<Match> matches = new ArrayList<>(){{add(TestUtils.getMatch());}};
        List<MatchDto> expected = new ArrayList<>(){{add(TestUtils.getMatchDto());}};

        assertEquals( expected, MATCH_MAPPER.toMatchDto(matches) );
    }
}
