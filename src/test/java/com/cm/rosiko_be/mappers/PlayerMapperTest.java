package com.cm.rosiko_be.mappers;

import com.cm.openapi.model.*;
import com.cm.rosiko_be.mission.Mission01;
import com.cm.rosiko_be.player.Player;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.cm.openapi.model.CardType.*;
import static com.cm.openapi.model.Color.RED;
import static com.cm.rosiko_be.mappers.PlayerMapper.PLAYER_MAPPER;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PlayerMapperTest {

    @Test
    void toPlayerDtoTest(){
        PlayerDto actualPlayerDto = PLAYER_MAPPER.toPlayerDto(getPlayer());
        PlayerDto expectedPlayerDto = getPlayerDto();
        assertEquals(expectedPlayerDto, actualPlayerDto);
    }

    @Test
    void toPlayerDtosTest(){
        List<Player> players = new ArrayList<>();
        players.add(getPlayer());

        List<PlayerDto> actualPlayerDtos = PLAYER_MAPPER.toPlayerDtos(players);

        List<PlayerDto> expectedPlayerDtos = new ArrayList<>();
        expectedPlayerDtos.add(getPlayerDto());

        assertEquals(expectedPlayerDtos, actualPlayerDtos);
    }


    private Player getPlayer(){
        Player player = new Player("4", "Player4");
        player.setColor(RED);
        player.setAvailableArmies(0);
        player.setMission(new Mission01(1L));
        player.setArmiesPlacedThisTurn(0);
        player.setMustDrawACard(false);
        player.setCards(getCards());
        player.setActive(true);
        player.setDefeatedPlayers(getPlayers(3));
        return player;
    }

    private PlayerDto getPlayerDto(){
        PlayerDto playerDto = new PlayerDto();
        List<String> defeatedPlayersId = new ArrayList<>();
        defeatedPlayersId.add("1");
        defeatedPlayersId.add("2");
        defeatedPlayersId.add("3");

        MissionDto missionDto = new MissionDto();
        missionDto.setId(1L);
        missionDto.setDescription("Capture Europe, Oceania and one other continent.");

        playerDto.setId("4");
        playerDto.setName("Player4");
        playerDto.setColor(RED);
        playerDto.setAvailableArmies(0);
        playerDto.setMission(missionDto);
        playerDto.setArmiesPlacedThisTurn(0);
        playerDto.setMustDrawACard(false);
        playerDto.setCards(getCards());
        playerDto.setIsActive(true);
        playerDto.setDefeatedPlayersId(defeatedPlayersId);


        return playerDto;
    }

    private Set<Player> getPlayers(int quantity){
        Set<Player> players = new HashSet<>();
        for(int i=1; i<=quantity; i++){
            Player player = new Player(i+"", "Player"+i);
            players.add(player);
        }

        return players;
    }

    private List<Card> getCards(){
        List<Card> cards = new ArrayList<>();

        Card card1 = new Card();
        card1.setId(1L);
        card1.setSelected(false);
        card1.setCardType(COW);
        card1.setTerritoryId("IT");
        card1.setTerritoryName("Italy");
        cards.add(card1);

        Card card2 = new Card();
        card2.setId(2L);
        card2.setSelected(true);
        card2.setCardType(FARMER);
        card2.setTerritoryId("DE");
        card2.setTerritoryName("Germany");
        cards.add(card2);

        Card card3 = new Card();
        card3.setId(3L);
        card3.setSelected(false);
        card3.setCardType(JOLLY);
        cards.add(card3);

        return cards;
    }
}
