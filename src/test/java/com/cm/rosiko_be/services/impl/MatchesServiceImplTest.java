package com.cm.rosiko_be.services.impl;

import com.cm.openapi.model.*;
import com.cm.rosiko_be.TestUtils;
import com.cm.rosiko_be.dao.GameMapDao;
import com.cm.rosiko_be.exceptions.GameRulesException;
import com.cm.rosiko_be.map.GameMap;
import com.cm.rosiko_be.map.territory.Territory;
import com.cm.rosiko_be.match.Match;
import com.cm.rosiko_be.mission.Mission;
import com.cm.rosiko_be.mission.Mission01;
import com.cm.rosiko_be.mission.MissionsService;
import com.cm.rosiko_be.model.User;
import com.cm.rosiko_be.player.Player;
import com.cm.rosiko_be.repositories.MatchRepository;
import com.cm.rosiko_be.services.StageService;
import com.cm.rosiko_be.utils.CardsUtil;
import com.cm.rosiko_be.utils.RulesUtil;
import com.cm.rosiko_be.utils.SelectableTerritoriesUtil;
import com.cm.rosiko_be.utils.TimeUtil;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import java.time.LocalDateTime;
import java.util.*;

import static com.cm.openapi.model.MatchStage.*;
import static com.cm.openapi.model.MatchState.*;
import static com.cm.rosiko_be.TestUtils.*;
import static com.cm.rosiko_be.mappers.MatchMapper.MATCH_MAPPER;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
@TestPropertySource(locations = "classpath:game-rules.properties")
class MatchesServiceImplTest {


    @InjectMocks
    MatchServiceImpl service;

    @Mock
    MatchRepository matchRepository;

    @Mock
    UserServiceImpl userService;

    @Mock
    StageService turnService;

    @Mock
    RulesUtil rulesUtil;

    @Mock
    CardsUtil cardsUtil;

    @Mock
    SelectableTerritoriesUtil selectableTerritories;

    @Mock
    MissionsService missionsService;

    @Mock
    GameMapDao gameMapDao;

    @Mock
    TimeUtil timeUtil;


    @Test
    void testNewMatch() {
        NewMatchRequest request = new NewMatchRequest();
        request.setName("name");
        request.setPassword("password");

        Match newMatch = new Match();
        newMatch.setName("name");
        newMatch.setPassword("password");
        newMatch.setState(WAITING);
        newMatch.setMap(getGameMap(2));
        newMatch.setTurn(0);
        newMatch.setStage(INITIAL_PLACEMENT);
        newMatch.setMovementConfirmed(false);
        newMatch.setArmiesWereAssigned(false);
        newMatch.setDate(LocalDateTime.of(2024,7,13, 17, 24));

        MatchDto expected = MATCH_MAPPER.toMatchDto(newMatch);

        when(matchRepository.findAll()).thenReturn(getMatches(2));
        when(gameMapDao.getGameMap()).thenReturn(getGameMap(2));
        when(timeUtil.now()).thenReturn(LocalDateTime.of(2024,7,13, 17, 24));

        assertEquals(expected, service.newMatch(request));

        verify(matchRepository).insert((Match) any());
    }

    @Test
    void testGetMatches(){
        Set<MatchState> statuses = new HashSet<>();
        statuses.add(READY);
        statuses.add(WAITING);

        List<Match> matches = getMatches(2);
        matches.getFirst().setState(READY);
        matches.getLast().setState(WAITING);


        when(matchRepository.findByStateIn(statuses)).thenReturn(matches);

        assertEquals(MATCH_MAPPER.toMatchDto(matches), service.getMatches(statuses));
    }

    @Test
    void testGetMatchesWithoutStatus(){

        List<Match> matches = getMatches(2);
        matches.getFirst().setState(READY);
        matches.getLast().setState(WAITING);

        List<MatchDto> expected = MATCH_MAPPER.toMatchDto(matches);


        when(matchRepository.findAll()).thenReturn(matches);

        assertEquals(expected, service.getMatches(new HashSet<>()));
        assertEquals(expected, service.getMatches(null));
    }

    @Test
    void testJoinMatch(){
        String matchId = "1";
        Match match = getMatch(matchId);

        User user = new User();
        user.setId("user_id");
        user.setName("player_name");

        when(matchRepository.findById(matchId)).thenReturn(Optional.of(match));
        when(userService.getUser()).thenReturn(user);

        service.joinMatch(matchId);

        verify(userService, times(1)).setCurrentMatch(match);
        verify(matchRepository, times(1)).save(match);

        Player newPlayer = null;
        for(Player player : match.getPlayers()){
            if(user.getId().equals(player.getId())) newPlayer = player;
        }

        assertNotNull(newPlayer);
        assertEquals(user.getId(), newPlayer.getId());
        assertEquals(user.getName(), newPlayer.getName());
        assertTrue(newPlayer.isActive());
        assertNotNull(newPlayer.getColor());
    }

    @Test
    void testJoinNotExistingMatch(){
        String matchId = "1";

        when(matchRepository.findById(matchId)).thenThrow(NoSuchElementException.class);

        assertThrows(NoSuchElementException.class, ()->service.joinMatch(matchId));
    }

    @Test
    void testLeavesMatch(){
        String matchId = "1";
        Match match = TestUtils.getMatch(matchId, 3);

        User user = new User();
        user.setId("user_id");
        user.setName("name");
        user.setCurrentMatch(match);

        Player player = match.getPlayers().getFirst();
        player.setName(user.getName());
        player.setId(user.getId());


        when(matchRepository.findById(matchId)).thenReturn(Optional.of(match));
        when(userService.getUser()).thenReturn(user);

        service.leavesMatch(matchId);

        verify(userService, times(1)).removeCurrentMatch(user);
        verify(matchRepository, times(1)).save(match);
    }

    @Test
    void testUpdateMatch(){
        Match match = TestUtils.getMatch("1");

        service.updateMatch(match);

        verify(matchRepository).save(match);
    }

    @Test
    void testGetMatch(){
        String matchId = "1";
        Match match = TestUtils.getMatch(matchId);
        MatchDto matchDto = MATCH_MAPPER.toMatchDto(match);

        when(matchRepository.findById(matchId)).thenReturn(Optional.of(match));

        assertEquals(matchDto, service.getMatch(matchId));
    }

    @Test
    void testStartMatch(){
        String matchId="1";
        int numberOfPlayers = 4;
        int numberOfTerritories = 8;
        int expectedNumberOfTerritoriesForEachPlayer = 2;
        int numberOfJolly = 2;
        int minimumArmiesInTerritories = 1;
        int expectedAvailableArmies = 28;
        Match match = getReadyMatch(matchId, numberOfPlayers, numberOfTerritories);
        Mission expectedMission = new Mission01(0);

        when(matchRepository.findById(matchId)).thenReturn(Optional.of(match));
        when(rulesUtil.getMinimumPlayers()).thenReturn(3);
        when(rulesUtil.getMaximumPlayers()).thenReturn(6);
        when(rulesUtil.getMinimumArmiesInTerritories()).thenReturn(minimumArmiesInTerritories);
        when(rulesUtil.getNumberOfJolly()).thenReturn(numberOfJolly);
        when(missionsService.getMissions()).thenReturn(getMissions(numberOfPlayers));

        service.startMatch(matchId);


        for(Player player : match.getPlayers()){
            Set<Territory> playerTerritories = match.getMap().getTerritoriesByOwner(player.getId());

            assertEquals(expectedAvailableArmies, player.getAvailableArmies());
            assertEquals(expectedMission, player.getMission());
            assertEquals(expectedNumberOfTerritoriesForEachPlayer, playerTerritories.size());
        }

        assertEquals(match.getPlayers().getFirst(), match.getPlayerOnDuty());
        assertEquals(INITIAL_PLACEMENT, match.getStage());
        assertEquals(STARTED, match.getState());
        assertEquals(numberOfTerritories + numberOfJolly, match.getCards().size());
        assertEquals(numberOfJolly, countJolly(match.getCards()));
        assertTrue(checkPlacedArmies(match.getMap().getTerritories(), minimumArmiesInTerritories));
    }

    @Test
    void testStartMatchWithWrongNumberOfPlayers(){

        String matchId="1";
        int numberOfPlayers = 2;
        int numberOfTerritories = 8;
        int numberOfJolly = 2;
        int minimumArmiesInTerritories = 1;
        Match match = getReadyMatch(matchId, numberOfPlayers, numberOfTerritories);

        when(matchRepository.findById(matchId)).thenReturn(Optional.of(match));
        when(rulesUtil.getMinimumPlayers()).thenReturn(3);
        when(rulesUtil.getMaximumPlayers()).thenReturn(6);
        when(rulesUtil.getMinimumArmiesInTerritories()).thenReturn(minimumArmiesInTerritories);
        when(rulesUtil.getNumberOfJolly()).thenReturn(numberOfJolly);
        when(missionsService.getMissions()).thenReturn(getMissions(numberOfPlayers));

        assertThrows(GameRulesException.class, ()->service.startMatch(matchId));

        numberOfPlayers = 7;
        match = getReadyMatch(matchId, numberOfPlayers, numberOfTerritories);
        match.setMap(getGameMap(numberOfTerritories));

        assertThrows(GameRulesException.class, ()->service.startMatch(matchId));
    }

    @Test
    void testPlaySet(){
        Match match = getStartedMatch("1", 0,10, PLACEMENT);
        Set<Card> cards = new HashSet<>();
        Set<Long> cardsId = new HashSet<>();
        Player player = new Player("1", "player");
        match.addPlayer(player);
        match.setPlayerOnDuty(player);

        for(int i=1; i<=3; i++){
            Card card = new Card();
            card.setId((long)i);

            cards.add(card);
            cardsId.add((long)i);
        }

        player.setCards(new ArrayList<>(cards));


        when(matchRepository.findById("1")).thenReturn(Optional.of(match));
        when(cardsUtil.getArmiesBonus(match, cards)).thenReturn(8);

        service.playCards("1", cardsId);

        assertEquals(8, player.getAvailableArmies());
        assertTrue(player.getCards().isEmpty());
    }

    @Test
    void testPlaySetInWrongStage(){
        Match match = getStartedMatch("1", 1,10, ATTACK);
        Set<Card> cards = new HashSet<>();
        Set<Long> cardsId = new HashSet<>();
        Player player = new Player("1", "player");
        match.addPlayer(player);
        match.setPlayerOnDuty(player);

        for(int i=1; i<=3; i++){
            Card card = new Card();
            card.setId((long)i);

            cards.add(card);
            cardsId.add((long)i);
        }

        player.setCards(cards.stream().toList());


        when(matchRepository.findById("1")).thenReturn(Optional.of(match));
        when(cardsUtil.getArmiesBonus(match, cards)).thenReturn(8);

        assertThrows(GameRulesException.class, ()->service.playCards("1", cardsId));
    }

    @Test
    void testPlaceArmies(){
        Match match = getStartedMatch("1", 1, 10, PLACEMENT);
        Player player = match.getPlayers().getFirst();
        GameMap map = match.getMap();
        Territory territory1 = map.getTerritory("1");
        Territory territory2 = map.getTerritory("2");

        player.setAvailableArmies(3);
        match.setPlayerOnDuty(player);

        territory1.setOwner(player);
        territory1.setPlacedArmies(1);
        territory2.setOwner(player);
        territory2.setPlacedArmies(3);

        ArmiesToPlaceDto armiesToPlace1Dto = new ArmiesToPlaceDto();
        armiesToPlace1Dto.setTerritoryId("1");
        armiesToPlace1Dto.setQuantity(2);
        ArmiesToPlaceDto armiesToPlace2Dto = new ArmiesToPlaceDto();
        armiesToPlace2Dto.setTerritoryId("2");
        armiesToPlace2Dto.setQuantity(1);
        Set<ArmiesToPlaceDto> setArmiesToPlace = Set.of(armiesToPlace1Dto, armiesToPlace2Dto);

        when(matchRepository.findById("1")).thenReturn(Optional.of(match));

        service.placeArmies("1", setArmiesToPlace);

        assertEquals(0, player.getAvailableArmies());
        assertEquals(3, territory1.getPlacedArmies());
        assertEquals(4, territory2.getPlacedArmies());
    }

    @Test
    void placeArmies_InWrongMatchStage_ThrowAGameRulesException(){
        Match match = getStartedMatch("1", 1, 10, ATTACK);
        Player player = match.getPlayers().getFirst();
        GameMap map = match.getMap();
        Territory territory1 = map.getTerritory("1");

        player.setAvailableArmies(3);
        match.setPlayerOnDuty(player);

        territory1.setOwner(player);
        territory1.setPlacedArmies(1);

        ArmiesToPlaceDto armiesToPlace1Dto = new ArmiesToPlaceDto();
        armiesToPlace1Dto.setTerritoryId("1");
        armiesToPlace1Dto.setQuantity(2);
        Set<ArmiesToPlaceDto> setArmiesToPlace = Set.of(armiesToPlace1Dto);

        when(matchRepository.findById("1")).thenReturn(Optional.of(match));

        assertThrows(GameRulesException.class, ()->service.placeArmies("1", setArmiesToPlace));
    }

    @Test
    void placeArmies_InTerritoryNotOwned_ThrowAGameRulesException(){
        Match match = getStartedMatch("1", 2, 10, PLACEMENT);
        Player player = match.getPlayers().getFirst();
        Player player2 = match.getPlayers().get(1);
        GameMap map = match.getMap();
        Territory territory1 = map.getTerritory("1");

        player.setAvailableArmies(3);
        match.setPlayerOnDuty(player);

        territory1.setOwner(player2);
        territory1.setPlacedArmies(1);

        ArmiesToPlaceDto armiesToPlace1Dto = new ArmiesToPlaceDto();
        armiesToPlace1Dto.setTerritoryId("1");
        armiesToPlace1Dto.setQuantity(2);
        Set<ArmiesToPlaceDto> setArmiesToPlace = Set.of(armiesToPlace1Dto);

        when(matchRepository.findById("1")).thenReturn(Optional.of(match));

        assertThrows(GameRulesException.class, ()->service.placeArmies("1", setArmiesToPlace));
    }

    @Test
    void placeArmies_PlayerHasntEnoughtAvailableArmies_ThrowAGameRulesException(){
        Match match = getStartedMatch("1", 1, 10, PLACEMENT);
        Player player = match.getPlayers().getFirst();
        GameMap map = match.getMap();
        Territory territory1 = map.getTerritory("1");
        Territory territory2 = map.getTerritory("2");

        player.setAvailableArmies(2);
        match.setPlayerOnDuty(player);

        territory1.setOwner(player);
        territory2.setOwner(player);

        ArmiesToPlaceDto armiesToPlace1Dto = new ArmiesToPlaceDto();
        armiesToPlace1Dto.setTerritoryId("1");
        armiesToPlace1Dto.setQuantity(2);
        ArmiesToPlaceDto armiesToPlace2Dto = new ArmiesToPlaceDto();
        armiesToPlace2Dto.setTerritoryId("2");
        armiesToPlace2Dto.setQuantity(1);
        Set<ArmiesToPlaceDto> setArmiesToPlace = Set.of(armiesToPlace1Dto, armiesToPlace2Dto);

        when(matchRepository.findById("1")).thenReturn(Optional.of(match));

        assertThrows(GameRulesException.class, ()->service.placeArmies("1", setArmiesToPlace));
    }


    private int countJolly(List<Card> cards){
        int jollyCounter = 0;
        for(Card card : cards){
            if(card.getCardType().equals(CardType.JOLLY)){
                jollyCounter++;
            }
        }
        return jollyCounter;
    }

    private boolean checkPlacedArmies(Set<Territory> territories, int armiesExpected){
        boolean armiesAreCorrect = true;
        for(Territory territory : territories){
            if(territory.getPlacedArmies() != armiesExpected){
                armiesAreCorrect = false;
                break;
            }
        }
        return armiesAreCorrect;
    }
}
