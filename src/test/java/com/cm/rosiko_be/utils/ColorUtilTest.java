package com.cm.rosiko_be.utils;

import com.cm.openapi.model.Color;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static com.cm.openapi.model.Color.*;
import static org.junit.jupiter.api.Assertions.*;

class ColorUtilTest {

    @Test
    void getRandomColorShouldNotReturnAnExcludedColor(){
        List<Color> excludedColors = getExcludedColors();
        Color color = ColorUtil.getRandomColor(excludedColors);
        assertFalse(excludedColors.contains(color));
    }

    @Test
    void getRandomColorThrowExceptionIfExcludedColorsAreAllColors(){
        List<Color> excludedColors = getAllColors();
        assertThrows(NoSuchElementException.class, ()-> ColorUtil.getRandomColor(excludedColors));
    }

    private List<Color> getExcludedColors(){
        List<Color> excludedColors = new ArrayList<>();
        excludedColors.add(GREEN);
        excludedColors.add(PURPLE);
        excludedColors.add(BLACK);
        return excludedColors;
    }

    private List<Color> getAllColors(){
        return new ArrayList<>(List.of(values()));
    }
}
