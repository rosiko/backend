    #Create the docker image
    docker build --tag rosiko_be_image:latest .
    #Create the network
    docker network create rosiko_network
    #Create and run new container from the image
    docker run -d --name rosiko_be_container -p 8081:8081 --network rosiko_network rosiko_be_image:latest